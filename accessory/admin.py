from django.contrib import admin
from .models import Accessory, AccessoryCategory, AccessorySubcategory

admin.site.register(Accessory)
admin.site.register(AccessoryCategory)
admin.site.register(AccessorySubcategory)