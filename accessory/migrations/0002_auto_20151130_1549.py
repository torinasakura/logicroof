# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessory',
            name='title',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0442\u0443\u044e\u0449\u0435\u0433\u043e'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='title_meta',
            field=models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0442\u0435\u0433\u0430 <metatitle>'),
        ),
        migrations.AlterField(
            model_name='accessorycategory',
            name='title_meta',
            field=models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0442\u0435\u0433\u0430 <metatitle>'),
        ),
        migrations.AlterField(
            model_name='accessorysubcategory',
            name='title_meta',
            field=models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0442\u0435\u0433\u0430 <metatitle>'),
        ),
    ]
