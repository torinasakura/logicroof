# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0002_auto_20151130_1549'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accessory',
            name='category',
        ),
        migrations.AddField(
            model_name='accessory',
            name='category',
            field=models.ManyToManyField(to='accessory.AccessoryCategory', verbose_name='\u0422\u0438\u043f \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
    ]
