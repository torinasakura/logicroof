# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0003_auto_20151130_1928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accessory',
            name='body',
            field=precise_bbcode.fields.BBCodeTextField(no_rendered_field=True, verbose_name='\u041f\u043e\u043b\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='category',
            field=models.ManyToManyField(to='accessory.AccessoryCategory', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='desc',
            field=precise_bbcode.fields.BBCodeTextField(no_rendered_field=True, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='img_main_ext',
            field=models.ImageField(upload_to=b'accessory', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='membrane',
            field=models.ManyToManyField(to='membrane.Membrane', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='subcategory',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='accessory.AccessorySubcategory'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='title',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='accessorysubcategory',
            name='desc',
            field=precise_bbcode.fields.BBCodeTextField(no_rendered_field=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='accessorysubcategory',
            name='title',
            field=models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435'),
        ),
    ]
