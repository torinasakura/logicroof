# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0008_accessorycategory_desc'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='accessory',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='accessorysubcategory',
            options={'ordering': ['order']},
        ),
    ]
