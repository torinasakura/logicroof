# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('accessory', '0012_auto_20160111_1508'),
    ]

    operations = [
        migrations.RenameField(
            model_name='accessory',
            old_name='img_main_ext',
            new_name='img_p',
        ),
        migrations.RenameField(
            model_name='accessory',
            old_name='img_main_ext_alt',
            new_name='img_p_alt',
        ),
        migrations.RenameField(
            model_name='accessory',
            old_name='img_main_ext_title',
            new_name='img_p_title',
        ),
    ]
