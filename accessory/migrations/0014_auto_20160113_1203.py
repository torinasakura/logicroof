# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0013_auto_20160111_1508'),
    ]

    operations = [
        migrations.AddField(
            model_name='accessorysubcategory',
            name='category',
            field=models.ManyToManyField(to='accessory.AccessoryCategory', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='accessory',
            name='subcategory',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', blank=True, to='accessory.AccessorySubcategory'),
        ),
    ]
