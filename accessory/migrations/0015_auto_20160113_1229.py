# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0014_auto_20160113_1203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accessorysubcategory',
            name='category',
        ),
        migrations.AlterField(
            model_name='accessory',
            name='subcategory',
            field=models.ForeignKey(verbose_name='\u041f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='accessory.AccessorySubcategory'),
        ),
    ]
