# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0015_solution_title_meta'),
        ('accessory', '0016_auto_20160113_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='accessory',
            name='solution',
            field=models.ManyToManyField(to='solution.Solution', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f', blank=True),
        ),
    ]
