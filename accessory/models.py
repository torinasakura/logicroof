# -*- coding: utf-8 -*-
from django.db import models
from precise_bbcode.fields import BBCodeTextField
from membrane.models import Membrane
from solution.models import Solution


class AccessoryCategory(models.Model):
    title = models.CharField(u'Преимущество', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    desc = models.TextField(u'Краткий текст описания', )

    def __unicode__(self):
        return self.title


class AccessorySubcategory(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    img_main = models.ImageField(u'Главное изображение', upload_to='accessory', blank=True)
    img_main_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    category = models.ManyToManyField(AccessoryCategory, verbose_name=u'Категория')
    desc = BBCodeTextField(u'Описание')

    def __unicode__(self):
        return ", ".join(c.title for c in self.category.all()) + ' - %s' % self.title

    class Meta:
        ordering = ['order']


class Accessory(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    img_p = models.ImageField(u'Изображение', upload_to='accessory', blank=True)
    img_p_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    img_main = models.ImageField(u'Главное изображение', upload_to='accessory', blank=True)
    img_main_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    subcategory = models.ManyToManyField(AccessorySubcategory, verbose_name=u'Подкатегория')
    desc = BBCodeTextField(u'Краткое описание')
    body = BBCodeTextField(u'Полное описание')
    solution = models.ManyToManyField(Solution, verbose_name=u'Используемые решения', blank=True)
    membrane = models.ManyToManyField(Membrane, verbose_name=u'Используемые мембраны', blank=True)
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']