from django.conf.urls import patterns, url
from .views import AccessoryDetail, AccessoryCategoryView, AccessorySubcategoryView


urlpatterns = patterns('',
                       url(r'^subcategory/(?P<slug>[-_\w]+)/', AccessorySubcategoryView.as_view(), {}, 'subcategory_category'),
                       url(r'^category/(?P<slug>[-_\w]+)/', AccessoryCategoryView.as_view(), {}, 'accessory_category'),
                       url(r'^(?P<slug>[-_\w]+)/$', AccessoryDetail.as_view(), {}, 'accessory'),
                       )
