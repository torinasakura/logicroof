from django.views.generic import DetailView
from document.models import Document
from .models import Accessory, AccessoryCategory, AccessorySubcategory


class AccessoryDetail(DetailView):
    model = Accessory
    context_object_name = 'item'
    template_name = 'accessory/index.html'

    def get_context_data(self, **kwargs):
        context = super(AccessoryDetail, self).get_context_data(**kwargs)
        context['other_accessories'] = Accessory.objects.filter().exclude(id=self.object.id)[:10]
        context['accessory_docs'] = Document.objects.filter(accessory=self.object).exclude(category_id=4)
        context['accessory_promotionals'] = Document.objects.filter(accessory=self.object, category_id=4)
        return context


class AccessoryCategoryView(DetailView):
    model = AccessoryCategory
    context_object_name = 'item'
    template_name = 'accessory/category.html'

    def get_context_data(self, **kwargs):
        context = super(AccessoryCategoryView, self).get_context_data(**kwargs)
        context['items'] = AccessorySubcategory.objects.filter(category=self.object)
        context['sc'] = True
        return context


class AccessorySubcategoryView(DetailView):
    model = AccessorySubcategory
    context_object_name = 'item'
    template_name = 'accessory/category.html'

    def get_context_data(self, **kwargs):
        context = super(AccessorySubcategoryView, self).get_context_data(**kwargs)
        context['items'] = Accessory.objects.filter(subcategory=self.object)
        return context