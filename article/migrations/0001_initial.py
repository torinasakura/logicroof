# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0001_initial'),
        ('solution', '__first__'),
        ('membrane', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('title_meta', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('slug', models.SlugField(verbose_name='URL')),
                ('img_main', models.ImageField(upload_to=b'article', verbose_name='\u0413\u043b\u0430\u0432\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('img_main_title', models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043f\u0440\u0435\u0432\u044c\u044e')),
                ('img_main_alt', models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('desc', models.TextField(verbose_name='\u0411\u0430\u0437\u043e\u0432\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('_body_rendered', models.TextField(null=True, editable=False, blank=True)),
                ('body', precise_bbcode.fields.BBCodeTextField(no_rendered_field=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
                ('add', models.DateTimeField(auto_now_add=True)),
                ('accessory', models.ManyToManyField(to='accessory.Accessory', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
                ('membrane', models.ManyToManyField(to='membrane.Membrane', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b')),
                ('solution', models.ManyToManyField(to='solution.Solution', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='ArticleCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0422\u0438\u043f \u0433\u0438\u0434\u0440\u043e\u0438\u0437\u044f\u043b\u044f\u0446\u0438\u0438')),
                ('title_meta', models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0442\u0435\u0433\u0430 <metatitle>')),
            ],
        ),
        migrations.CreateModel(
            name='NewsSubscribe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='type',
            field=models.ForeignKey(to='article.ArticleCategory'),
        ),
    ]
