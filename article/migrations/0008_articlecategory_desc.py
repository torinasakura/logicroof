# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0007_auto_20160111_0929'),
    ]

    operations = [
        migrations.AddField(
            model_name='articlecategory',
            name='desc',
            field=models.TextField(default='\u0437\u0430\u043f\u043e\u043b\u043d\u0438 \u043c\u0435\u043d\u044f', verbose_name='\u0411\u0430\u0437\u043e\u0432\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435'),
            preserve_default=False,
        ),
    ]
