# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0009_auto_20160111_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='add',
            field=models.DateTimeField(),
        ),
    ]
