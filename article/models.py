# -*- coding: utf-8 -*-
from django.db import models
from precise_bbcode.fields import BBCodeTextField
from membrane.models import Membrane
from solution.models import Solution
from accessory.models import Accessory


class ArticleCategory(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    desc = models.TextField(u'Базовое описание')

    def __unicode__(self):
        return self.title


class NewsSubscribe(models.Model):
    email = models.EmailField(u'Email')

    def __unicode__(self):
        return self.email


class Article(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    add = models.DateTimeField()
    img_p = models.ImageField(u'Изображение превью', upload_to='membrane', blank=True)
    img_p_title = models.CharField(u'Title-атрибут', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут', max_length=255, blank=True)
    img_main = models.ImageField(u'Главное изображение', upload_to='article', blank=True)
    img_main_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    desc = models.TextField(u'Базовое описание')
    type = models.ForeignKey(ArticleCategory, verbose_name=u'Категория')
    body = BBCodeTextField(u'Описание')
    membrane = models.ManyToManyField(Membrane, verbose_name=u'Используемые мембраны', blank=True)
    solution = models.ManyToManyField(Solution, verbose_name=u'Используемые решения', blank=True)
    accessory = models.ManyToManyField(Accessory, verbose_name=u'Используемые комплектующие', blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-add']