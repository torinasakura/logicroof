from django.conf.urls import url
from .views import ArticleDetail, ArticleCategoryView


urlpatterns = [
    url(r'^category/ll/(?P<slug>[-_\w]+)/(?P<o>[0-9]+)/(?P<l>[0-9]+)/$',
        ArticleCategoryView.as_view(template_name='article/ll.html'), {}, 'article_category'),
    url(r'^category/(?P<slug>[-_\w]+)/$', ArticleCategoryView.as_view(), {}, 'article_category'),
    url(r'^(?P<slug>[-_\w]+)/$', ArticleDetail.as_view(), {}, 'article'),
]
