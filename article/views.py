from django.views.generic import DetailView, View
from .models import Article, ArticleCategory
from django.shortcuts import render


class ArticleDetail(DetailView):
    model = Article
    context_object_name = 'item'
    template_name = 'article/index.html'

    def get_context_data(self, **kwargs):
        context = super(ArticleDetail, self).get_context_data(**kwargs)
        context['other_article'] = Article.objects.filter(type=self.object.type).exclude(id=self.object.id)
        return context


class ArticleCategoryView(View):
    template_name = 'article/category.html'

    def get(self, request, slug, l=12, o=0):
        l = int(l)
        o = int(o)
        context = dict()
        objects = Article.objects.filter(type__slug=slug)[o:o + l]
        item = ArticleCategory.objects.get(slug=slug)
        context['items'] = objects
        context['item'] = item
        return render(request, self.template_name, context)