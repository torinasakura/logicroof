from accessory.models import Accessory
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    class Meta:
        model = Accessory
        fields = ('title', 'title_meta', 'slug', 'order', 'img_main', 'img_main_title', 'img_main_alt', 'img_p',
                  'img_p_title', 'img_p_alt', 'desc', 'body', 'membrane', 'solution', 'subcategory')
        widgets = {'img_main_ext': ImageWidget(), 'img_main': ImageWidget()}