from django.conf.urls import patterns, url
from .views import AccessoryList, AccessoryEdit, AccessoryAdd, AccessoryDel


urlpatterns = patterns('',
                       url(r'^$', AccessoryList.as_view(), {}, 'accessory'),
                       url(r'^edit/(?P<pk>[0-9]+)/', AccessoryEdit.as_view(), {}, 'accessory_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', AccessoryDel.as_view(), {}, 'accessory_del'),
                       url(r'^add/', AccessoryAdd.as_view(), {}, 'accessory_add'),
)