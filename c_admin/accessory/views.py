# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from accessory.models import Accessory
from .forms import EditAddForm


class AccessoryList(ListView):
    template_name = 'c_admin/accessory/index.html'
    context_object_name = "accessorys"
    model = Accessory


class AccessoryDel(DeleteView):
    model = Accessory
    template_name = 'c_admin/accessory/del.html'
    success_url = '/c_admin/accessory/'


class AccessoryEdit(UpdateView):
    form_class = EditAddForm
    model = Accessory
    template_name = 'c_admin/accessory/edit.html'
    success_url = '/c_admin/accessory/'


class AccessoryAdd(CreateView):
    form_class = EditAddForm
    model = Accessory
    template_name = 'c_admin/accessory/edit.html'
    success_url = '/c_admin/accessory/'
