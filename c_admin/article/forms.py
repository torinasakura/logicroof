from article.models import Article
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    class Meta:
        model = Article
        fields = ('title', 'title_meta', 'slug', 'add', 'img_main', 'img_main_title', 'img_main_alt',  'type', 'desc', 'body',
                  'membrane', 'solution', 'accessory')
        widgets = {'img_main_ext': ImageWidget(), 'img_main': ImageWidget()}
