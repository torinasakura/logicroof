from django.conf.urls import patterns, url
from .views import ArticleList, ArticleEdit, ArticleAdd, ArticleDel


urlpatterns = patterns('',
                       url(r'^$', ArticleList.as_view(), {}, 'article'),
                       url(r'^edit/(?P<pk>[0-9]+)/', ArticleEdit.as_view(), {}, 'article_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', ArticleDel.as_view(), {}, 'article_del'),
                       url(r'^add/', ArticleAdd.as_view(), {}, 'article_add'),
)