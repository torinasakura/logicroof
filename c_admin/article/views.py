# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from article.models import Article
from .forms import EditAddForm


class ArticleList(ListView):
    template_name = 'c_admin/article/index.html'
    context_object_name = "articles"
    model = Article


class ArticleDel(DeleteView):
    model = Article
    template_name = 'c_admin/article/del.html'
    success_url = '/c_admin/article/'


class ArticleEdit(UpdateView):
    form_class = EditAddForm
    model = Article
    template_name = 'c_admin/article/edit.html'
    success_url = '/c_admin/article/'


class ArticleAdd(CreateView):
    form_class = EditAddForm
    model = Article
    template_name = 'c_admin/article/edit.html'
    success_url = '/c_admin/article/'
