from membrane.models import Brand
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    class Meta:
        model = Brand
        fields = ('title', 'title_meta', 'slug', 'order', 'hero', 'img_p', 'img_p_title', 'img_p_alt', 'img_main',
                  'img_main_title', 'img_main_alt', 'img_logo', 'img_logo_title', 'img_logo_alt', 'definition', 'desc',
                  'info', 'adv')
        widgets = {'img_p': ImageWidget(), 'img_main': ImageWidget(), 'img_logo': ImageWidget()}





