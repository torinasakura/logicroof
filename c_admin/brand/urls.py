from django.conf.urls import patterns, url
from .views import BrandList, BrandEdit, BrandAdd, BrandDel


urlpatterns = patterns('',
                       url(r'^$', BrandList.as_view(), {}, 'brand'),
                       url(r'^edit/(?P<pk>[0-9]+)/', BrandEdit.as_view(), {}, 'brand_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', BrandDel.as_view(), {}, 'brand_del'),
                       url(r'^add/', BrandAdd.as_view(), {}, 'brand_add'),
)