# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from membrane.models import Brand
from .forms import EditAddForm


class BrandList(ListView):
    template_name = 'c_admin/brand/index.html'
    context_object_name = "brands"
    model = Brand


class BrandDel(DeleteView):
    model = Brand
    template_name = 'c_admin/brand/del.html'
    success_url = '/c_admin/brand/'


class BrandEdit(UpdateView):
    form_class = EditAddForm
    model = Brand
    template_name = 'c_admin/brand/edit.html'
    success_url = '/c_admin/brand/'


class BrandAdd(CreateView):
    form_class = EditAddForm
    model = Brand
    template_name = 'c_admin/brand/edit.html'
    success_url = '/c_admin/brand/'
