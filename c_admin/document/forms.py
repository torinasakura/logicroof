from document.models import Document
from ..utils.forms import BaseModelForm


class EditAddForm(BaseModelForm):
    class Meta:
        model = Document
        fields = ('title', 'order', 'country', 'pdf', 'doc', 'dwg',  'ppt', 'category', 'type',
                  'membrane', 'solution', 'accessory')
