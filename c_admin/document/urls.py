from django.conf.urls import patterns, url
from .views import DocumentList, DocumentEdit, DocumentAdd, DocumentDel


urlpatterns = patterns('',
                       url(r'^$', DocumentList.as_view(), {}, 'document'),
                       url(r'^edit/(?P<pk>[0-9]+)/', DocumentEdit.as_view(), {}, 'document_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', DocumentDel.as_view(), {}, 'document_del'),
                       url(r'^add/', DocumentAdd.as_view(), {}, 'document_add'),
)