# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from document.models import Document
from .forms import EditAddForm


class DocumentList(ListView):
    template_name = 'c_admin/document/index.html'
    context_object_name = "documents"
    model = Document


class DocumentDel(DeleteView):
    model = Document
    template_name = 'c_admin/document/del.html'
    success_url = '/c_admin/document/'


class DocumentEdit(UpdateView):
    form_class = EditAddForm
    model = Document
    template_name = 'c_admin/document/edit.html'
    success_url = '/c_admin/document/'


class DocumentAdd(CreateView):
    form_class = EditAddForm
    model = Document
    template_name = 'c_admin/document/edit.html'
    success_url = '/c_admin/document/'
