from document.models import DocumentGroup
from ..utils.forms import BaseModelForm


class EditAddForm(BaseModelForm):
    class Meta:
        model = DocumentGroup
        fields = ('title', 'order', 'fire', 'conf', 'mail')