from django.conf.urls import patterns, url
from .views import DocumentGroupList, DocumentGroupEdit, DocumentGroupAdd, DocumentGroupDel


urlpatterns = patterns('',
                       url(r'^$', DocumentGroupList.as_view(), {}, 'document'),
                       url(r'^edit/(?P<pk>[0-9]+)/', DocumentGroupEdit.as_view(), {}, 'document_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', DocumentGroupDel.as_view(), {}, 'document_del'),
                       url(r'^add/', DocumentGroupAdd.as_view(), {}, 'document_add'),
)