# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from document.models import DocumentGroup
from .forms import EditAddForm


class DocumentGroupList(ListView):
    template_name = 'c_admin/document_group/index.html'
    context_object_name = "document_groups"
    model = DocumentGroup


class DocumentGroupDel(DeleteView):
    model = DocumentGroup
    template_name = 'c_admin/document_group/del.html'
    success_url = '/c_admin/document_group/'


class DocumentGroupEdit(UpdateView):
    form_class = EditAddForm
    model = DocumentGroup
    template_name = 'c_admin/document_group/edit.html'
    success_url = '/c_admin/document_group/'


class DocumentGroupAdd(CreateView):
    form_class = EditAddForm
    model = DocumentGroup
    template_name = 'c_admin/document_group/edit.html'
    success_url = '/c_admin/document_group/'
