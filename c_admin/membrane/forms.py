from membrane.models import Membrane
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    class Meta:
        model = Membrane
        fields = ('title', 'slug', 'order', 'img_p', 'img_p_title', 'img_p_alt', 'img_main', 'img_main_title', 'img_main_alt',
                  'desc', 'target', 'adv', 'polymer', 'reinforce', 'colors', 'pack_text', 'pack', 'store',
                  'brand', 'category', 'thickness', 'strength', 'strengthp', 'elongation', 'absorp', 'flex', 'resist',
                  'comb')
        widgets = {'img_p': ImageWidget(), 'img_main': ImageWidget()}
