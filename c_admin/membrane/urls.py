from django.conf.urls import patterns, url
from .views import MembraneList, MembraneEdit, MembraneAdd, MembraneDel


urlpatterns = patterns('',
                       url(r'^$', MembraneList.as_view(), {}, 'membrane'),
                       url(r'^edit/(?P<pk>[0-9]+)/', MembraneEdit.as_view(), {}, 'membrane_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', MembraneDel.as_view(), {}, 'membrane_del'),
                       url(r'^add/', MembraneAdd.as_view(), {}, 'membrane_add'),
)