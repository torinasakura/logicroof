# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from membrane.models import Membrane
from .forms import EditAddForm


class MembraneList(ListView):
    template_name = 'c_admin/membrane/index.html'
    context_object_name = "membranes"
    model = Membrane


class MembraneDel(DeleteView):
    model = Membrane
    template_name = 'c_admin/membrane/del.html'
    success_url = '/c_admin/membrane/'


class MembraneEdit(UpdateView):
    form_class = EditAddForm
    model = Membrane
    template_name = 'c_admin/membrane/edit.html'
    success_url = '/c_admin/membrane/'


class MembraneAdd(CreateView):
    form_class = EditAddForm
    model = Membrane
    template_name = 'c_admin/membrane/edit.html'
    success_url = '/c_admin/membrane/'
