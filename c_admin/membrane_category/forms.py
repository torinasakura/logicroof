from membrane.models import MembraneCategory
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    class Meta:
        model = MembraneCategory
        fields = ('title', 'slug', 'order', 'img_p', 'img_p_title', 'img_p_alt', 'title_meta', 'desc', 'desc_ext', 'info')
        widgets = {'img_p': ImageWidget()}