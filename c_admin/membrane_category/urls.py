from django.conf.urls import patterns, url
from .views import MembraneCategoryList, MembraneCategoryEdit, MembraneCategoryAdd


urlpatterns = patterns('',
                       url(r'^$', MembraneCategoryList.as_view(), {}, 'membrane_category'),
                       url(r'^edit/(?P<pk>[0-9]+)/', MembraneCategoryEdit.as_view(), {}, 'membrane_category_edit'),
                       url(r'^add/', MembraneCategoryAdd.as_view(), {}, 'membrane_category_add'),
)