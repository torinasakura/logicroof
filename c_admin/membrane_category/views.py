# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView

from membrane.models import MembraneCategory
from .forms import EditAddForm


class MembraneCategoryList(ListView):
    template_name = 'c_admin/membrane_category/index.html'
    context_object_name = "membrane_category"
    model = MembraneCategory


class MembraneCategoryEdit(UpdateView):
    form_class = EditAddForm
    model = MembraneCategory
    template_name = 'c_admin/membrane_category/edit.html'
    success_url = '/c_admin/membrane_category/'


class MembraneCategoryAdd(CreateView):
    form_class = EditAddForm
    model = MembraneCategory
    template_name = 'c_admin/membrane_category/edit.html'
    success_url = '/c_admin/membrane_category/'
