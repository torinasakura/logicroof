from object.models import Object, ObjectCity
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        super(EditAddForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Object
        fields = ('title', 'title_meta', 'slug', 'order', 'img_p', 'img_p_title', 'img_p_alt', 'img_main',
                  'img_main_title', 'img_main_alt', 'city', 'year', 'area', 'desc', 'type', 'category', 'solution',
                  'membrane')
        widgets = {'img_p': ImageWidget(), 'img_main': ImageWidget()}


