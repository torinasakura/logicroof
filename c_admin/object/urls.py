from django.conf.urls import patterns, url
from .views import ObjectList, ObjectEdit, ObjectAdd, ObjectDel


urlpatterns = patterns('',
                       url(r'^$', ObjectList.as_view(), {}, 'object'),
                       url(r'^edit/(?P<pk>[0-9]+)/', ObjectEdit.as_view(), {}, 'object_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', ObjectDel.as_view(), {}, 'object_del'),
                       url(r'^add/', ObjectAdd.as_view(), {}, 'object_add'),
)