# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from object.models import Object
from .forms import EditAddForm


class ObjectList(ListView):
    template_name = 'c_admin/object/index.html'
    context_object_name = "objects"
    model = Object


class ObjectDel(DeleteView):
    model = Object
    template_name = 'c_admin/object/del.html'
    success_url = '/c_admin/object/'


class ObjectEdit(UpdateView):
    form_class = EditAddForm
    model = Object
    template_name = 'c_admin/object/edit.html'
    success_url = '/c_admin/object/'


class ObjectAdd(CreateView):
    form_class = EditAddForm
    model = Object
    template_name = 'c_admin/object/edit.html'
    success_url = '/c_admin/object/'
