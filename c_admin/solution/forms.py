from solution.models import Solution
from ..utils.forms import BaseModelForm
from ..utils.widgets import ImageWidget


class EditAddForm(BaseModelForm):
    class Meta:
        model = Solution
        fields = ('title', 'title_meta', 'slug', 'order', 'img_p', 'img_p_title', 'img_p_alt', 'img_main', 'img_main_title',
                  'img_main_alt', 'category', 'desc', 'desc_ext', 'body', 'svg', 'membrane', 'adv')
        widgets = {'img_p': ImageWidget(), 'img_main_ext': ImageWidget()}