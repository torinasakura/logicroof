from django.conf.urls import patterns, url
from .views import SolutionList, SolutionEdit, SolutionAdd, SolutionDel


urlpatterns = patterns('',
                       url(r'^$', SolutionList.as_view(), {}, 'solution'),
                       url(r'^edit/(?P<pk>[0-9]+)/', SolutionEdit.as_view(), {}, 'solution_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', SolutionDel.as_view(), {}, 'solution_del'),
                       url(r'^add/', SolutionAdd.as_view(), {}, 'solution_add'),
)