# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from solution.models import Solution
from .forms import EditAddForm


class SolutionList(ListView):
    template_name = 'c_admin/solution/index.html'
    context_object_name = "solutions"
    model = Solution


class SolutionDel(DeleteView):
    model = Solution
    template_name = 'c_admin/solution/del.html'
    success_url = '/c_admin/solution/'


class SolutionEdit(UpdateView):
    form_class = EditAddForm
    model = Solution
    template_name = 'c_admin/solution/edit.html'
    success_url = '/c_admin/solution/'


class SolutionAdd(CreateView):
    form_class = EditAddForm
    model = Solution
    template_name = 'c_admin/solution/edit.html'
    success_url = '/c_admin/solution/'
