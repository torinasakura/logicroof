from django.conf.urls import patterns, url, include
from django.contrib.auth.views import login, logout


urlpatterns = patterns('',
                       url(r'^$', include('c_admin.membrane.urls')),
                       url(r'^membrane/', include('c_admin.membrane.urls')),
                       url(r'^membrane_category/', include('c_admin.membrane_category.urls')),
                       url(r'^brand/', include('c_admin.brand.urls')),
                       url(r'^solution/', include('c_admin.solution.urls')),
                       url(r'^object/', include('c_admin.object.urls')),
                       url(r'^accessory/', include('c_admin.accessory.urls')),
                       url(r'^article/', include('c_admin.article.urls')),
                       url(r'^document/', include('c_admin.document.urls')),
                       url(r'^document_group/', include('c_admin.document_group.urls')),
                       url(r'^video/', include('c_admin.video.urls')),
                       url(r'^login/$', login, {'template_name': 'c_admin/login.html'}, 'login'),
                       url(r'^logout/$', logout),
)