from django import forms
from slugify import slugify
import StringIO
from django.core.files.base import ContentFile
import os
from core.settings import MEDIA_ROOT


def image_res(data, width):
    from PIL import Image

    image_field = data
    image_file = StringIO.StringIO(image_field.read())
    img = Image.open(image_file)
    wp = (width / float(img.size[0]))
    hs = int((float(img.size[1]) * float(wp)))
    if img.format == 'JPEG':
        if img.mode not in ('L', 'RGB'):
            img = img.convert('RGB')
        if img.size[0] > width:
            image = img.resize((width, hs), Image.ANTIALIAS)
        else:
            image = img
        image_file = StringIO.StringIO()
        image.save(image_file, 'JPEG', quality=50)
    if img.format == 'PNG':
        if img.mode not in ('L', 'RGBA'):
            img = img.convert('RGBA')
        if img.size[0] > width:
            image = img.resize((width, hs), Image.ANTIALIAS)
        else:
            image = img
        image_file = StringIO.StringIO()
        image.save(image_file, 'PNG', quality=50)
    return image_file


class BaseModelForm(forms.ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super(BaseModelForm, self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control', 'placeholder': self.fields[field].label
            })

    def as_div(self):
        return self._html_output(
            normal_row=u'<div class="form-group">%(label)s %(errors)s %(field)s %(help_text)s </div>',
            error_row=u'<div class="form-group has-warning">%s</div>',
            row_ender='</div>',
            help_text_html=u'<div class="help-block">%s</div>',
            errors_on_separate_row=False)

    def save(self):
        instance = super(BaseModelForm, self).save()
        if hasattr(instance, 'slug'):
            if instance.slug == '':
                instance.slug = slugify(instance.title, to_lower=True)[:50]
            else:
                instance.slug = slugify(instance.slug, to_lower=True)[:50]
        if hasattr(instance, 'img_p'):
            if instance.img_p.name != '':
                name = instance.img_p.name
                instance.img_p.save(instance.img_p.name, ContentFile(image_res(instance.img_p, 1024).getvalue()))
                os.remove(os.path.join(MEDIA_ROOT, name))
        if hasattr(instance, 'img_main'):
            if instance.img_main.name != '':
                name = instance.img_main.name
                instance.img_main.save(instance.img_main.name, ContentFile(image_res(instance.img_main, 3072).getvalue()))
                os.remove(os.path.join(MEDIA_ROOT, name))
        instance.save()
        return instance