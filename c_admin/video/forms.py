from video.models import Video
from ..utils.forms import BaseModelForm


class EditAddForm(BaseModelForm):
    class Meta:
        model = Video
        fields = ('title', 'order', 'desc', 'subject', 'url')