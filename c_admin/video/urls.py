from django.conf.urls import patterns, url
from .views import VideoList, VideoEdit, VideoAdd, VideoDel


urlpatterns = patterns('',
                       url(r'^$', VideoList.as_view(), {}, 'video'),
                       url(r'^edit/(?P<pk>[0-9]+)/', VideoEdit.as_view(), {}, 'video_edit'),
                       url(r'^del/(?P<pk>[0-9]+)/', VideoDel.as_view(), {}, 'video_del'),
                       url(r'^add/', VideoAdd.as_view(), {}, 'video_add'),
)