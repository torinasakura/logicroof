# - coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from video.models import Video
from .forms import EditAddForm


class VideoList(ListView):
    template_name = 'c_admin/video/index.html'
    context_object_name = "videos"
    model = Video


class VideoDel(DeleteView):
    model = Video
    template_name = 'c_admin/video/del.html'
    success_url = '/c_admin/video/'


class VideoEdit(UpdateView):
    form_class = EditAddForm
    model = Video
    template_name = 'c_admin/video/edit.html'
    success_url = '/c_admin/video/'


class VideoAdd(CreateView):
    form_class = EditAddForm
    model = Video
    template_name = 'c_admin/video/edit.html'
    success_url = '/c_admin/video/'
