from django.conf.urls import url
from .views import MembraneDetailAccessory, MembraneDetailObject, MembraneDetailOther, MembraneDetailSolution, \
    BrandDetailObject, BrandDetailSolution, CategoryDetailArticle, CategoryDetailBrand, CategoryDetailObject, \
    CategoryDetailSolution, ObjectDetailBrand, SolutionDetailAccessory, SolutionDetailObject, SolutionDetailSolution, \
    AccessoryDetailAccessory


urlpatterns = [
    url(r'^membrane/(?P<slug>[-_\w]+)/accessory/', MembraneDetailAccessory.as_view(), {}),
    url(r'^membrane/(?P<slug>[-_\w]+)/object/', MembraneDetailObject.as_view(), {}),
    url(r'^membrane/(?P<slug>[-_\w]+)/other/', MembraneDetailOther.as_view(), {}),
    url(r'^membrane/(?P<slug>[-_\w]+)/solution/', MembraneDetailSolution.as_view(), {}),

    url(r'^brand/(?P<slug>[-_\w]+)/object/', BrandDetailObject.as_view(), {}),
    url(r'^brand/(?P<slug>[-_\w]+)/solution/', BrandDetailSolution.as_view(), {}),

    url(r'^category/(?P<slug>[-_\w]+)/article/', CategoryDetailArticle.as_view(), {}),
    url(r'^category/(?P<slug>[-_\w]+)/brand/', CategoryDetailBrand.as_view(), {}),
    url(r'^category/(?P<slug>[-_\w]+)/object/', CategoryDetailObject.as_view(), {}),
    url(r'^category/(?P<slug>[-_\w]+)/solution/', CategoryDetailSolution.as_view(), {}),

    url(r'^object/(?P<slug>[-_\w]+)/objects/', ObjectDetailBrand.as_view(), {}),

    url(r'^solution/(?P<slug>[-_\w]+)/accessory/', SolutionDetailAccessory.as_view(), {}),
    url(r'^solution/(?P<slug>[-_\w]+)/object/', SolutionDetailObject.as_view(), {}),
    url(r'^solution/(?P<slug>[-_\w]+)/solution/', SolutionDetailSolution.as_view(), {}),

    url(r'^accessory/(?P<slug>[-_\w]+)/accessory/', AccessoryDetailAccessory.as_view(), {}),
]
