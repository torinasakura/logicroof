from django.views.generic import DetailView, ListView, View
from membrane.models import Membrane, Brand, MembraneCategory
from document.models import Document
from accessory.models import Accessory
from object.models import Object
from solution.models import Solution
from article.models import Article
from django.shortcuts import render


class MembraneDetailAccessory(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        membrane = Membrane.objects.get(slug=slug)
        context['objects'] = Accessory.objects.filter(membrane=membrane)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class MembraneDetailSolution(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        membrane = Membrane.objects.get(slug=slug)
        context['objects'] = Solution.objects.filter(membrane=membrane)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class MembraneDetailObject(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        membrane = Membrane.objects.get(slug=slug)
        context['objects'] = Object.objects.filter(membrane=membrane)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class MembraneDetailOther(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug, offset=0):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        membrane = Membrane.objects.get(slug=slug)
        context['objects'] = Membrane.objects.filter(brand=membrane.brand).exclude(id=membrane.id)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class BrandDetailSolution(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        brand = Brand.objects.get(slug=slug)
        membranes = brand.membrane_set.all()
        context['objects'] = Solution.objects.filter(membrane__in=membranes).distinct()[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class BrandDetailObject(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        brand = Brand.objects.get(slug=slug)
        membranes = brand.membrane_set.all()
        context['objects'] = Object.objects.filter(membrane__in=membranes).distinct()[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class CategoryDetailSolution(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        category = MembraneCategory.objects.get(slug=slug)
        membranes = category.membrane_set.all()
        context['objects'] = Solution.objects.filter(membrane__in=membranes)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class CategoryDetailArticle(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        category = MembraneCategory.objects.get(slug=slug)
        membranes = category.membrane_set.all()
        context['objects'] = Article.objects.filter(membrane__in=membranes)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class CategoryDetailObject(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        category = MembraneCategory.objects.get(slug=slug)
        membranes = category.membrane_set.all()
        context['objects'] = Object.objects.filter(membrane__in=membranes)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class CategoryDetailBrand(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        category = MembraneCategory.objects.get(slug=slug)
        membranes = category.membrane_set.all()
        context['objects'] = Brand.objects.filter(membrane__in=membranes)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class ObjectDetailBrand(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        objecti = Object.objects.get(slug=slug)
        context['objects'] = Object.objects.filter(category=objecti.category).exclude(id=objecti.id)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class SolutionDetailAccessory(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        solution = Solution.objects.get(slug=slug)
        membranes = solution.membrane.all()
        context['objects'] = Accessory.objects.filter(membrane__in=membranes)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class SolutionDetailObject(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        solution = Solution.objects.get(slug=slug)
        context['objects'] = solution.object_set.filter()[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class SolutionDetailSolution(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        solution = Solution.objects.get(slug=slug)
        context['objects'] = Solution.objects.filter(category=solution.category).exclude(id=solution.id)[
                             offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)


class AccessoryDetailAccessory(View):
    template_name = 'carousel/index.html'

    def get(self, request, slug):
        offset = int(request.GET.get('offset', 0))
        context = dict()
        accessory = Accessory.objects.get(slug=slug)
        context['objects'] = Accessory.objects.filter().exclude(id=accessory.id)[offset:10 + offset]
        context['cl'] = context['objects'][0].__class__.__name__
        return render(request, self.template_name, context)