from django.contrib import admin
from .models import Document, DocumentCategory, DocumentGroup, DocumentType, DocumentCountry


admin.site.register(Document)
admin.site.register(DocumentCategory)
admin.site.register(DocumentGroup)
admin.site.register(DocumentType)
admin.site.register(DocumentCountry)