# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accessory', '0001_initial'),
        ('solution', '__first__'),
        ('membrane', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('slug', models.SlugField(verbose_name='URL')),
                ('pdf', models.FileField(upload_to=b'doc', verbose_name='pdf')),
                ('doc', models.FileField(upload_to=b'doc', verbose_name='doc')),
                ('dwg', models.FileField(upload_to=b'doc', verbose_name='dwg')),
                ('ppt', models.FileField(upload_to=b'doc', verbose_name='ppt')),
                ('add', models.DateTimeField(auto_now_add=True)),
                ('accessory', models.ManyToManyField(to='accessory.Accessory', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
            ],
        ),
        migrations.CreateModel(
            name='DocumentCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.CreateModel(
            name='DocumentGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
                ('add', models.DateTimeField(auto_now_add=True)),
                ('conf', models.ForeignKey(related_name='document_conf', to='document.Document')),
                ('fire', models.ForeignKey(related_name='document_fire', to='document.Document')),
                ('mail', models.ForeignKey(related_name='document_mail', to='document.Document')),
            ],
        ),
        migrations.CreateModel(
            name='DocumentType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.AddField(
            model_name='document',
            name='category',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u043e\u0431\u044a\u0435\u043a\u0442\u0430', to='document.DocumentCategory'),
        ),
        migrations.AddField(
            model_name='document',
            name='membrane',
            field=models.ManyToManyField(to='membrane.Membrane', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b'),
        ),
        migrations.AddField(
            model_name='document',
            name='solution',
            field=models.ManyToManyField(to='solution.Solution', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='document',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430', to='document.DocumentType'),
        ),
    ]
