# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='doc',
            field=models.FileField(upload_to=b'doc', verbose_name='doc', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='dwg',
            field=models.FileField(upload_to=b'doc', verbose_name='dwg', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='pdf',
            field=models.FileField(upload_to=b'doc', verbose_name='pdf', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='ppt',
            field=models.FileField(upload_to=b'doc', verbose_name='ppt', blank=True),
        ),
    ]
