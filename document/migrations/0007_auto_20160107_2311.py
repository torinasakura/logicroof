# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0006_auto_20160107_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentcategory',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
    ]
