# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0007_auto_20160107_2311'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='document',
            options={'ordering': ['add']},
        ),
        migrations.AlterModelOptions(
            name='documentcategory',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='documentgroup',
            options={'ordering': ['order']},
        ),
    ]
