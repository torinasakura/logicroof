# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0008_auto_20160107_2349'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentcategory',
            name='order',
            field=models.IntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='documentgroup',
            name='order',
            field=models.IntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
    ]
