# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0010_auto_20160108_0039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='accessory',
            field=models.ManyToManyField(to='accessory.Accessory', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0442\u0443\u044e\u0449\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u043e\u0431\u044a\u0435\u043a\u0442\u0430', blank=True, to='document.DocumentCategory'),
        ),
        migrations.AlterField(
            model_name='document',
            name='membrane',
            field=models.ManyToManyField(to='membrane.Membrane', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='solution',
            field=models.ManyToManyField(to='solution.Solution', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='document',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430', blank=True, to='document.DocumentType'),
        ),
    ]
