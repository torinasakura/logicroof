# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0019_objectcountry_slug'),
        ('document', '0012_documentcategory_title_meta'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='city',
            field=models.ForeignKey(default=1, verbose_name='\u0413\u043e\u0440\u043e\u0434', to='object.ObjectCity'),
            preserve_default=False,
        ),
    ]
