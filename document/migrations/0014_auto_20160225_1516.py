# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0019_objectcountry_slug'),
        ('document', '0013_document_city'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='document',
            name='city',
        ),
        migrations.AddField(
            model_name='document',
            name='country',
            field=models.ForeignKey(default=1, verbose_name='\u0421\u0422\u0440\u0430\u043d\u0430', to='object.ObjectCountry'),
            preserve_default=False,
        ),
    ]
