# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0014_auto_20160225_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='country',
            field=models.ForeignKey(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', to='object.ObjectCountry'),
        ),
    ]
