# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('document', '0016_auto_20160228_1540'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='country',
            field=models.ForeignKey(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', to='document.DocumentCountry'),
        ),
        migrations.AddField(
            model_name='document',
            name='order',
            field=models.IntegerField(default=500,
                                      verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
        migrations.AddField(
            model_name='documenttype',
            name='order',
            field=models.IntegerField(default=500,
                                      verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
    ]
