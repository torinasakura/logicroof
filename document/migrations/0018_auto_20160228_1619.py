# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0017_auto_20160228_1614'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430', blank=True, to='document.DocumentType', null=True),
        ),
    ]
