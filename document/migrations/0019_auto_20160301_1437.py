# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0018_auto_20160228_1619'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='document',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='documenttype',
            options={'ordering': ['order']},
        ),
    ]
