# -*- coding: utf-8 -*-
from django.db import models
from membrane.models import Membrane
from solution.models import Solution
from accessory.models import Accessory
from djangosphinx.models import SphinxSearch


class DocumentType(models.Model):
    title = models.CharField(u'Название', max_length=255)
    order = models.IntegerField(u'Порядок сортировки', default=500)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class DocumentCountry(models.Model):
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u'URL', blank=True)

    def __unicode__(self):
        return self.title


class DocumentCategory(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class Document(models.Model):
    title = models.CharField(u'Название', max_length=255)
    order = models.IntegerField(u'Порядок сортировки', default=500)
    pdf = models.FileField(u'pdf', upload_to='doc', blank=True)
    doc = models.FileField(u'doc', upload_to='doc', blank=True)
    dwg = models.FileField(u'dwg', upload_to='doc', blank=True)
    ppt = models.FileField(u'ppt', upload_to='doc', blank=True)
    category = models.ForeignKey(DocumentCategory, verbose_name=u'Категория объекта', blank=True)
    type = models.ForeignKey(DocumentType, verbose_name=u'Тип документа', blank=True, null=True)
    membrane = models.ManyToManyField(Membrane, verbose_name=u'Используемые мембраны', blank=True)
    solution = models.ManyToManyField(Solution, verbose_name=u'Используемые решения', blank=True)
    accessory = models.ManyToManyField(Accessory, verbose_name=u'Используемые комплектующие', blank=True)
    country = models.ForeignKey(DocumentCountry, verbose_name=u'Страна')
    add = models.DateTimeField(auto_now_add=True)
    search = SphinxSearch(index='documents', weights={'title': 100})

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class DocumentGroup(models.Model):
    title = models.CharField(u'Название', max_length=255)
    order = models.IntegerField(u'Порядок сортировки')
    fire = models.ForeignKey(Document, related_name='document_fire')
    conf = models.ForeignKey(Document, related_name='document_conf')
    mail = models.ForeignKey(Document, related_name='document_mail')
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']