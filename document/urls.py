from django.conf.urls import url
from .views import DocumentGroupList, DocumentDetailView


urlpatterns = [
    url(r'^configurations/$', DocumentGroupList.as_view(), {}, 'doc_group'),
    url(r'^(?P<slug>[-_\w]+)/$', DocumentDetailView.as_view(), {'t': 'all', 'c': 'all', 'w': 'none'}, 'doc_category'),
    url(r'^(?P<slug>[-_\w]+)/(?P<c>[-_\w]+)/(?P<t>[-_ \w]+)/$', DocumentDetailView.as_view(), {'w': 'none'},
        'doc_category'),
    url(r'^(?P<slug>[-_\w]+)/(?P<c>[-_\w]+)/(?P<t>[-_\w]+)/(?P<w>[-_ \w]+)/$', DocumentDetailView.as_view(), {},
        'doc_category'),
    url(r'^raw/(?P<slug>[-_\w]+)/(?P<c>[-_\w]+)/(?P<t>[-_\w]+)/(?P<w>[-_ \w]+)/$',
        DocumentDetailView.as_view(template_name='document/raw.html'), {}, 'doc_raw_category'),
]

