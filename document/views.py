from django.views.generic import ListView, View
from .models import DocumentCategory, DocumentGroup, Document, DocumentType, DocumentCountry
from django.shortcuts import render


class DocumentGroupList(ListView):
    template_name = 'document/group.html'
    model = DocumentGroup
    context_object_name = 'items'


class DocumentDetailView(View):
    template_name = 'document/index.html'

    def get(self, request, slug, c, t, w):
        context = dict()
        dc = DocumentCategory.objects.get(slug=slug)
        documents = Document.objects.filter(category=dc)
        if c != 'all':
            documents = documents.filter(country__id=c)
        if t != 'all':
            documents = documents.filter(type__id=t)
        if w != 'none':
            result = Document.search.query(w)
            id_list = [item.id for item in result]
            documents = documents.filter(id__in=id_list)
        context['item'] = dc
        context['documents'] = documents
        context['type'] = DocumentType.objects.filter(document__set_in=documents).distinct()
        context['type_all'] = DocumentType.objects.filter()
        context['country'] = DocumentCountry.objects.filter(document__set_in=documents).distinct()
        context['country_all'] = DocumentCountry.objects.filter()
        context['js'] = {'t': t, 'c': c, 'w': w}
        return render(request, self.template_name, context)