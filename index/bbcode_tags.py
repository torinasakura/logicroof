from precise_bbcode.bbcode.tag import BBCodeTag
from precise_bbcode.tag_pool import tag_pool


class BarBBCodeTagH1(BBCodeTag):
    name = 'h1'
    definition_string = '[h1]{TEXT}[/h1]'
    format_string = '<h1>{TEXT}</h1>'


tag_pool.register_tag(BarBBCodeTagH1)


class BarBBCodeTagH2(BBCodeTag):
    name = 'h2'
    definition_string = '[h2]{TEXT}[/h2]'
    format_string = '<h2>{TEXT}</h2>'


tag_pool.register_tag(BarBBCodeTagH2)


class BarBBCodeTagH3(BBCodeTag):
    name = 'h3'
    definition_string = '[h3]{TEXT}[/h3]'
    format_string = '<h3>{TEXT}</h3>'


tag_pool.register_tag(BarBBCodeTagH3)


class BarBBCodeTagH4(BBCodeTag):
    name = 'h4'
    definition_string = '[h4]{TEXT}[/h4]'
    format_string = '<h4>{TEXT}</h4>'


tag_pool.register_tag(BarBBCodeTagH4)


class BarBBCodeTagH5(BBCodeTag):
    name = 'h5'
    definition_string = '[h5]{TEXT}[/h5]'
    format_string = '<h5>{TEXT}</h5>'


tag_pool.register_tag(BarBBCodeTagH5)


class BarBBCodeTagA(BBCodeTag):
    name = 'a'
    definition_string = '[a={URL}]{TEXT}[/a]'
    format_string = '<a href="{URL}" class="hyperlink">{TEXT}</a>'


tag_pool.register_tag(BarBBCodeTagA)


class BarBBCodeTagP(BBCodeTag):
    name = 'p'
    definition_string = '[p]{TEXT}[/p]'
    format_string = '<p>{TEXT}</p>'


tag_pool.register_tag(BarBBCodeTagP)


class BarBBCodeTagSp(BBCodeTag):
    name = 'sp'
    definition_string = '[sp]{TEXT}[/sp]'
    format_string = '<sup>{TEXT}</sup>'


tag_pool.register_tag(BarBBCodeTagSp)


class BarBBCodeTagSb(BBCodeTag):
    name = 'sb'
    definition_string = '[sb]{TEXT}[/sb]'
    format_string = '<sub>{TEXT}</sub>'


tag_pool.register_tag(BarBBCodeTagSb)

class BarBBCodeTagUl(BBCodeTag):
    name = 'ul'
    definition_string = '[ul]{TEXT}[/ul]'
    format_string = '<ul>{TEXT}</ul>'


tag_pool.register_tag(BarBBCodeTagUl)

class BarBBCodeTagOl(BBCodeTag):
    name = 'ol'
    definition_string = '[ol]{TEXT}[/ol]'
    format_string = '<ol>{TEXT}</ol>'

tag_pool.register_tag(BarBBCodeTagOl)

class BarBBCodeTagLi(BBCodeTag):
    name = 'li'
    definition_string = '[*]{TEXT}[/*]'
    format_string = '<li>{TEXT}</li>'

tag_pool.register_tag(BarBBCodeTagLi)