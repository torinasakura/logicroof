from django import template

register = template.Library()


@register.inclusion_tag('tt/carousel.html')
def carousel(objects, special=''):
    cl = objects[0].__class__.__name__
    cs = special
    objects = objects[:10]
    return {'objects': objects,
            'cl': cl,
            'cs': cs}
