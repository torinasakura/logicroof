from django import template

register = template.Library()


@register.inclusion_tag('tt/carousel.html')
def carousel(objects):
    cl = objects[0].__class__.__name__
    return {'objects': objects,
            'cl': cl}
