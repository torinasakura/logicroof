from django import template
from document.models import DocumentType
from collections import OrderedDict


register = template.Library()


@register.inclusion_tag('tt/doc.html')
def doc(objects, name, docpage=False):
    count = int(str(len(objects))[-1])
    docs = objects
    cat = DocumentType.objects.filter(document__in=objects)

    c_list = OrderedDict()
    for category in cat:
        c_list[category] = OrderedDict()
        for doc in docs:
            if doc.type == category:
                c_list[category][doc] = doc
    return {'objects': c_list,
            'name': name,
            'count': count,
            'docpage': docpage}
