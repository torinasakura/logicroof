from django import template
from membrane.models import Brand, Membrane, MembraneCategory
from solution.models import SolutionCategory
from object.models import ObjectCategory
from accessory.models import AccessoryCategory
from document.models import DocumentCategory
from collections import OrderedDict

register = template.Library()


@register.inclusion_tag('tt/menu.html')
def menu():
    mc = MembraneCategory.objects.all()
    sol = SolutionCategory.objects.filter()
    obj = ObjectCategory.objects.filter()
    acc = AccessoryCategory.objects.filter()
    doc = DocumentCategory.objects.filter()

    c_list = OrderedDict()
    for category in mc:
        membranes = category.membrane_set.filter()
        brands = Brand.objects.filter(membrane__in=membranes).distinct()
        c_list[category] = OrderedDict()
        for brand in brands:
            bmemb = brand.membrane_set.all()
            c_list[category][brand] = bmemb
    return {'c_list': c_list,
            'sol': sol,
            'obj': obj,
            'acc': acc,
            'doc': doc}
