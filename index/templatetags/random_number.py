from django import template
from random import randint

register = template.Library()


@register.assignment_tag()
def random_number(length=6):
    return randint(10 ** (length - 1), (10 ** (length) - 1))