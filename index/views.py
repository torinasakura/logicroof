# - coding: utf-8
from django.views.generic import View
from django.shortcuts import redirect, render
from membrane.models import Brand, MembraneCategory
from article.models import Article
from object.models import Object
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


class Index(View):
    template_name = 'index.html'

    def get(self, request):
        brands = Brand.objects.all()
        articles = Article.objects.filter(type__id=1)
        news = Article.objects.filter(type__id=2)
        objects = Object.objects.all()
        membranecat_1 = MembraneCategory.objects.filter(id=1).values('desc_ext')
        membranecat_2 = MembraneCategory.objects.filter(id=2).values('desc_ext')
        membranecat_3 = MembraneCategory.objects.filter(id=3).values('desc_ext')
        context = {'brands': brands,
                   'articles': articles,
                   'news': news,
                   'objects': objects,
                   'membranecat_1': membranecat_1,
                   'membranecat_2': membranecat_2,
                   'membranecat_3': membranecat_3}
        return render(request, self.template_name, context)


class ContactForm(View):
    def post(self, request):
        context = {}
        print request.POST
        context['fieldCity'] = request.POST.get('fieldCity')
        context['fieldComment'] = request.POST.get('fieldComment')
        context['fieldCounrty'] = request.POST.get('fieldCounrty')
        context['fieldEmail'] = request.POST.get('fieldEmail')
        context['fieldName'] = request.POST.get('fieldName')
        context['fieldOrg'] = request.POST.get('fieldOrg')
        context['fieldTel'] = request.POST.get('fieldTel')
        context['targ'] = request.POST.get('targ')
        message = render_to_string('email.html', context)
        send_mail(settings.EMAIL_TITLE,
                  message,
                  settings.EMAIL_HOST_USER,
                  [settings.EMAIL_SUBJECT],
                  fail_silently=False
        )
        return redirect('/contacts/')


