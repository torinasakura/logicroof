from django.contrib import admin
from .models import Membrane, MembraneAdv, MembraneCategory, MembraneColors, MembranePack, MembrBrandAdv, Brand, MembraneReinforce

admin.site.register(Membrane)
admin.site.register(MembraneAdv)
admin.site.register(MembraneCategory)
admin.site.register(Brand)
admin.site.register(MembraneColors)
admin.site.register(MembranePack)
admin.site.register(MembrBrandAdv)
admin.site.register(MembraneReinforce)