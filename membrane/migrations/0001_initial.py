# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('slug', models.SlugField(verbose_name='URL')),
                ('img_p', models.ImageField(upload_to=b'brand',
                                            verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043f\u0440\u0435\u0432\u044c\u044e')),
                ('img_p_title', models.CharField(max_length=255,
                                                 verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043f\u0440\u0435\u0432\u044c\u044e')),
                ('img_p_alt', models.CharField(max_length=255,
                                               verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('img_main', models.ImageField(upload_to=b'brand',
                                               verbose_name='\u0413\u043b\u0430\u0432\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('img_main_title', models.CharField(max_length=255,
                                                    verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043f\u0440\u0435\u0432\u044c\u044e')),
                ('img_main_alt', models.CharField(max_length=255,
                                                  verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('img_logo', models.ImageField(upload_to=b'brand',
                                               verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043b\u043e\u0433\u043e\u0442\u0438\u043f\u0430')),
                ('img_logo_title', models.CharField(max_length=255,
                                                    verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('img_logo_alt', models.CharField(max_length=255,
                                                  verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('definition', models.CharField(max_length=255,
                                                verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u043e\u043f\u0440\u0435\u0434\u0435\u043b\u0435\u043d\u0438\u044f \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('titletext', models.CharField(max_length=255,
                                               verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u0431\u0440\u0435\u043d\u0434\u0430')),
            ],
        ),
        migrations.CreateModel(
            name='Membrane',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b')),
                ('slug', models.SlugField(verbose_name='URL')),
                ('img_p', models.ImageField(upload_to=b'membrane',
                                            verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043f\u0440\u0435\u0432\u044c\u044e')),
                ('img_p_title',
                 models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442')),
                ('img_p_alt',
                 models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442')),
                ('img_main', models.ImageField(upload_to=b'membrane',
                                               verbose_name='\u0413\u043b\u0430\u0432\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('img_main_title',
                 models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442')),
                ('img_main_alt',
                 models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442')),
                ('title_text', models.TextField(
                    verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f')),
                ('_target_rendered', models.TextField(null=True, editable=False, blank=True)),
                ('target', precise_bbcode.fields.BBCodeTextField(no_rendered_field=True,
                                                                 verbose_name='\u041d\u0430\u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('polymer', models.CharField(max_length=16,
                                             verbose_name='\u0422\u0438\u043f \u043f\u043e\u043b\u0438\u043c\u0435\u0440\u0430',
                                             choices=[(b'pvh', '\u041f\u0412\u0425'), (b'tpo', '\u0422\u041f\u041e')])),
                ('thickness',
                 models.FloatField(verbose_name='\u0422\u043e\u043b\u0449\u0438\u043d\u0430, \u043c\u043c.')),
                ('strength', models.IntegerField(
                    verbose_name='\u041f\u0440\u043e\u0447\u043d\u043e\u0441\u0442\u044c, \u041c\u041f\u0430')),
                ('elongation', models.IntegerField(
                    verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u0443\u0434\u043b\u0438\u043d\u0435\u043d\u0438\u0435, %')),
                ('absorp', models.FloatField(
                    verbose_name='\u0412\u043e\u0434\u043e\u043f\u043e\u0433\u043b\u0430\u0449\u0435\u043d\u0438\u0435, %')),
                ('flex', models.IntegerField(verbose_name='\u0413\u0438\u0431\u043a\u043e\u0441\u0442\u044c')),
                ('resist', models.CharField(max_length=255,
                                            verbose_name='\u0421\u043e\u043f\u0440\u043e\u0442\u0438\u0432\u043b\u0435\u043d\u0438\u0435 \u0441\u0442\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u043e\u043c\u0443 \u043f\u0440\u043e\u0434\u0430\u0432\u043b\u0438\u0432\u0430\u043d\u0438\u044e')),
                ('comb', models.CharField(max_length=255,
                                          verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430 \u0433\u043e\u0440\u044e\u0447\u0435\u0441\u0442\u0438')),
                ('pack_text', models.TextField(
                    verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438')),
                ('_store_rendered', models.TextField(null=True, editable=False, blank=True)),
                ('store', precise_bbcode.fields.BBCodeTextField(no_rendered_field=True,
                                                                verbose_name='\u0423\u0441\u043b\u043e\u0432\u0438\u044f \u0445\u0440\u0430\u043d\u0435\u043d\u0438\u044f')),
                ('add', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='MembraneAdv',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.CreateModel(
            name='MembraneCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='\u0422\u0438\u043f \u0433\u0438\u0434\u0440\u043e\u0438\u0437\u044f\u043b\u044f\u0446\u0438\u0438')),
                ('title_meta', models.CharField(max_length=255,
                                                verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0442\u0435\u0433\u0430 <metatitle>')),
            ],
        ),
        migrations.CreateModel(
            name='MembraneColors',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('color_name', models.CharField(max_length=255,
                                                verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0446\u0432\u0435\u0442\u0430')),
                ('color_hex', models.CharField(max_length=255, verbose_name='\u0426\u0432\u0435\u0442 \u0432 HEX')),
            ],
        ),
        migrations.CreateModel(
            name='MembranePack',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('thick', models.FloatField(
                    verbose_name='\u0422\u043e\u043b\u0449\u0438\u043d\u0430 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b, \u043c\u043c')),
                ('w', models.IntegerField(
                    verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430 \u0440\u0443\u043b\u043e\u043d\u0430, \u043c')),
                ('l', models.IntegerField(
                    verbose_name='\u0414\u043b\u0438\u043d\u0430 \u0440\u0443\u043b\u043e\u043d\u0430, \u043c')),
                ('rols', models.IntegerField(
                    verbose_name='\u041a\u043e\u043b-\u0432\u043e \u0440\u0443\u043b\u043e\u043d\u043e\u0432 \u043d\u0430 \u043f\u0430\u043b\u0435\u0442\u0435')),
            ],
        ),
        migrations.CreateModel(
            name='MembraneReinforce',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255,
                                           verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.CreateModel(
            name='MembrBrandAdv',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ico', models.ImageField(upload_to=b'brand',
                                          verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('title', models.CharField(max_length=255,
                                           verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u0430')),
            ],
        ),
        migrations.AddField(
            model_name='membrane',
            name='adv',
            field=models.ManyToManyField(to='membrane.MembraneAdv',
                                         verbose_name='\u0422\u0435\u043a\u0441\u0442\u043e\u0432\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432'),
        ),
        migrations.AddField(
            model_name='membrane',
            name='brand',
            field=models.ForeignKey(
                verbose_name='\u0411\u0440\u0435\u043d\u0434 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b',
                to='membrane.Brand'),
        ),
        migrations.AddField(
            model_name='membrane',
            name='category',
            field=models.ForeignKey(
                verbose_name='\u0422\u0438\u043f \u0433\u0438\u0434\u0440\u043e\u0438\u0437\u044f\u043b\u044f\u0446\u0438\u0438',
                to='membrane.MembraneCategory'),
        ),
        migrations.AddField(
            model_name='membrane',
            name='colors',
            field=models.ManyToManyField(to='membrane.MembraneColors', verbose_name='\u0426\u0432\u0435\u0442'),
        ),
        migrations.AddField(
            model_name='membrane',
            name='pack',
            field=models.ManyToManyField(to='membrane.MembranePack',
                                         verbose_name='\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438 \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438'),
        ),
        migrations.AddField(
            model_name='membrane',
            name='reinforce',
            field=models.ManyToManyField(to='membrane.MembraneReinforce',
                                         verbose_name='\u0422\u0438\u043f\u044b \u0430\u0440\u043c\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='brand',
            name='adv',
            field=models.ManyToManyField(to='membrane.MembrBrandAdv',
                                         verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u0430'),
        ),
    ]
