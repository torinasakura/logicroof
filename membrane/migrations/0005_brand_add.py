# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):
    dependencies = [
        ('membrane', '0004_auto_20151202_1527'),
    ]

    operations = [
        migrations.AddField(
            model_name='brand',
            name='add',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 7, 14, 44, 58, 887859, tzinfo=utc),
                                       auto_now_add=True),
            preserve_default=False,
        ),
    ]
