# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('membrane', '0005_brand_add'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membranepack',
            name='w',
            field=models.FloatField(
                verbose_name='\u0428\u0438\u0440\u0438\u043d\u0430 \u0440\u0443\u043b\u043e\u043d\u0430, \u043c'),
        ),
    ]
