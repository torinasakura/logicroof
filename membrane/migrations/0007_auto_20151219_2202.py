# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):
    dependencies = [
        ('membrane', '0006_auto_20151219_2151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membrane',
            name='pack',
            field=models.ManyToManyField(to='membrane.MembranePack',
                                         verbose_name='\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438 \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438',
                                         blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='pack_text',
            field=models.TextField(
                verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0443\u043f\u0430\u043a\u043e\u0432\u043a\u0438',
                blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='store',
            field=precise_bbcode.fields.BBCodeTextField(no_rendered_field=True,
                                                        verbose_name='\u0423\u0441\u043b\u043e\u0432\u0438\u044f \u0445\u0440\u0430\u043d\u0435\u043d\u0438\u044f',
                                                        blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='strength',
            field=models.IntegerField(
                verbose_name='\u041f\u0440\u043e\u0447\u043d\u043e\u0441\u0442\u044c, \u041c\u043f\u0430, \u0432\u0434\u043e\u043b\u044c \u0440\u0443\u043b\u043e\u043d\u0430'),
        ),
    ]
