# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('membrane', '0007_auto_20151219_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membrane',
            name='category',
            field=models.ForeignKey(
                verbose_name='\u0422\u0438\u043f \u0433\u0438\u0434\u0440\u043e\u0438\u0437\u043e\u043b\u044f\u0446\u0438\u0438',
                to='membrane.MembraneCategory'),
        ),
    ]
