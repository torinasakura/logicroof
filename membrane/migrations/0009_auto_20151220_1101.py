# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('membrane', '0008_auto_20151219_2207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membrane',
            name='thickness',
            field=models.CharField(max_length=255,
                                   verbose_name='\u0422\u043e\u043b\u0449\u0438\u043d\u0430, \u043c\u043c'),
        ),
    ]
