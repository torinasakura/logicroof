# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0010_auto_20151221_0437'),
    ]

    operations = [
        migrations.RenameField(
            model_name='membrane',
            old_name='title_text',
            new_name='desc',
        ),
    ]
