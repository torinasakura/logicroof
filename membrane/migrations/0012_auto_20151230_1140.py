# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0011_auto_20151229_1349'),
    ]

    operations = [
        migrations.AddField(
            model_name='membranecategory',
            name='img_p',
            field=models.ImageField(default='', upload_to=b'membrane', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043f\u0440\u0435\u0432\u044c\u044e'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membranecategory',
            name='img_p_alt',
            field=models.CharField(default='', max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membranecategory',
            name='img_p_title',
            field=models.CharField(default='', max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043f\u0440\u0435\u0432\u044c\u044e'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membranecategory',
            name='slug',
            field=models.SlugField(default='', verbose_name='URL'),
            preserve_default=False,
        ),
    ]
