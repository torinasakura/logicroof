# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0012_auto_20151230_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='membranecategory',
            name='desc',
            field=models.TextField(default='', verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f'),
            preserve_default=False,
        ),
    ]
