# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0016_auto_20160107_2246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membrane',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
        migrations.AlterField(
            model_name='membranecategory',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
    ]
