# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0017_auto_20160107_2311'),
    ]

    operations = [
        migrations.RenameField(
            model_name='brand',
            old_name='titletext',
            new_name='desc',
        ),
    ]
