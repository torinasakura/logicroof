# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0018_auto_20160107_2341'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='brand',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='membrane',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='membranecategory',
            options={'ordering': ['order']},
        ),
    ]
