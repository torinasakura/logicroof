# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0019_auto_20160107_2346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membrbrandadv',
            name='ico',
            field=models.ImageField(upload_to=b'brand', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
        ),
    ]
