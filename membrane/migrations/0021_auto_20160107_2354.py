# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0020_auto_20160107_2353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brand',
            name='order',
            field=models.IntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='order',
            field=models.IntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='membranecategory',
            name='order',
            field=models.IntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
        ),
    ]
