# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0024_auto_20160125_0920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='membrane',
            name='absorp',
            field=models.FloatField(null=True, verbose_name='\u0412\u043e\u0434\u043e\u043f\u043e\u0433\u043b\u0430\u0449\u0435\u043d\u0438\u0435, %', blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='elongation',
            field=models.IntegerField(null=True, verbose_name='\u041e\u0442\u043d\u043e\u0441\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u0443\u0434\u043b\u0438\u043d\u0435\u043d\u0438\u0435, %', blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='flex',
            field=models.IntegerField(null=True, verbose_name='\u0413\u0438\u0431\u043a\u043e\u0441\u0442\u044c', blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='strength',
            field=models.IntegerField(null=True, verbose_name='\u041f\u0440\u043e\u0447\u043d\u043e\u0441\u0442\u044c, \u041c\u043f\u0430, \u0432\u0434\u043e\u043b\u044c \u0440\u0443\u043b\u043e\u043d\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='membrane',
            name='strengthp',
            field=models.IntegerField(null=True, verbose_name='\u041f\u0440\u043e\u0447\u043d\u043e\u0441\u0442\u044c, \u041c\u043f\u0430, \u043f\u043e\u043f\u0435\u0440\u0435\u043a \u0440\u0443\u043b\u043e\u043d\u0430', blank=True),
        ),
    ]
