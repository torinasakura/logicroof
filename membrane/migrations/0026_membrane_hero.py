# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0025_auto_20160127_1006'),
    ]

    operations = [
        migrations.AddField(
            model_name='membrane',
            name='hero',
            field=models.CharField(max_length=255, verbose_name='Hero', blank=True),
        ),
    ]
