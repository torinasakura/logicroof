# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0026_membrane_hero'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='membrane',
            name='hero',
        ),
        migrations.AddField(
            model_name='brand',
            name='hero',
            field=models.CharField(max_length=255, verbose_name='Hero', blank=True),
        ),
    ]
