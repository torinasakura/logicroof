# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0029_auto_20160219_0527'),
    ]

    operations = [
        migrations.RenameField(
            model_name='brand',
            old_name='desccription',
            new_name='description',
        ),
    ]
