# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0031_auto_20160219_0557'),
    ]

    operations = [
        migrations.RenameField(
            model_name='brand',
            old_name='description',
            new_name='info',
        ),
    ]
