# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0032_auto_20160219_0602'),
    ]

    operations = [
        migrations.AddField(
            model_name='membranecategory',
            name='info',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0432 \u0442\u0435\u043b\u043e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='brand',
            name='info',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0432 \u0442\u0435\u043b\u043e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', blank=True),
        ),
        migrations.AlterField(
            model_name='membranecategory',
            name='desc',
            field=models.TextField(verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u0432 Hero'),
        ),
        migrations.AlterField(
            model_name='membranecategory',
            name='desc_ext',
            field=models.TextField(verbose_name='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f \u043d\u0430 \u0413\u043b\u0430\u0432\u043d\u0443\u044e', blank=True),
        ),
    ]
