# -*- coding: utf-8 -*-
from django.db import models
from precise_bbcode.fields import BBCodeTextField


class MembraneColors(models.Model):
    color_name = models.CharField(u'Название', max_length=255)
    color_hex = models.CharField(u'Цвет в HEX', max_length=255)

    def __unicode__(self):
        return self.color_name


class MembranePack(models.Model):
    thick = models.FloatField(u'Толщина мембраны, мм', )
    w = models.FloatField(u'Ширина рулона, м', )
    l = models.IntegerField(u'Длина рулона, м', )
    rols = models.IntegerField(u'Кол-во рулонов на палете', )

    def __unicode__(self):
        return u'%s; %s; %s; %s' % (self.thick, self.w, self.l, self.rols)


class MembraneCategory(models.Model):
    title = models.CharField(u'Тип гидроизяляции', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    desc = models.TextField(u'Краткий текст описания в Hero', )
    desc_ext = models.TextField(u'Краткий текст описания на Главную', blank=True)
    info = models.TextField(u'Описание в тело страницы', blank=True)
    img_p = models.ImageField(u'Изображение превью', upload_to='membrane', blank=True)
    img_p_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class MembraneAdv(models.Model):
    title = models.CharField(u'Преимущество', max_length=255)

    def __unicode__(self):
        return self.title


class MembraneReinforce(models.Model):
    title = models.CharField(u'Преимущество', max_length=255)

    def __unicode__(self):
        return self.title


class MembrBrandAdv(models.Model):
    ico = models.ImageField(u'Изображение', upload_to='brand', blank=True)
    title = models.CharField(u'Текст преимущества', max_length=255)

    def __unicode__(self):
        return self.title


class Brand(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    hero = models.CharField(u'Hero', max_length=255, blank=True)
    img_p = models.ImageField(u'Изображение превью', upload_to='brand', blank=True)
    img_p_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    img_main = models.ImageField(u'Главное изображение', upload_to='brand', blank=True)
    img_main_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    img_logo = models.ImageField(u'Изображение логотипа', upload_to='brand', blank=True)
    img_logo_title = models.CharField(u'Title-атрибут изображения', max_length=255, blank=True)
    img_logo_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    definition = models.CharField(u'Краткий текст определения бренда', max_length=255)
    desc = models.CharField(u'Краткий текст описания бренда', max_length=255)
    info = models.TextField(u'Описание в тело страницы', blank=True)
    adv = models.ManyToManyField(MembrBrandAdv, verbose_name=u'Преимущества')
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class Membrane(models.Model):
    POLYMER_TYPE = (
        ('pvh', u'ПВХ'),
        ('tpo', u'ТПО'),
    )
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    img_p = models.ImageField(u'Изображение превью', upload_to='membrane', blank=True)
    img_p_title = models.CharField(u'Title-атрибут', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут', max_length=255, blank=True)
    img_main = models.ImageField(u'Главное изображение', upload_to='membrane', blank=True)
    img_main_title = models.CharField(u'Title-атрибут', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут', max_length=255, blank=True)
    desc = models.TextField(u'Краткий текст описания', )
    target = BBCodeTextField(u'Назначение', )
    adv = models.ManyToManyField(MembraneAdv, verbose_name=u'Текстовое описание преимуществ')
    polymer = models.CharField(u'Тип полимера', choices=POLYMER_TYPE, max_length=16)
    reinforce = models.ManyToManyField(MembraneReinforce, verbose_name=u'Типы армирования')
    colors = models.ManyToManyField(MembraneColors, verbose_name=u'Цвет')
    pack_text = models.TextField(u'Описание упаковки', blank=True)
    pack = models.ManyToManyField(MembranePack, verbose_name=u'Характеристики упаковки', blank=True)
    store = BBCodeTextField(u'Условия хранения', blank=True)
    brand = models.ForeignKey(Brand, verbose_name=u'Бренд мембраны')
    category = models.ForeignKey(MembraneCategory, verbose_name=u'Тип гидроизоляции')
    thickness = models.CharField(u'Толщина, мм', max_length=255)
    strength = models.IntegerField(u'Прочность, Мпа, вдоль рулона', blank=True, null=True)
    strengthp = models.IntegerField(u'Прочность, Мпа, поперек рулона', blank=True, null=True)
    elongation = models.IntegerField(u'Относительное удлинение, %', blank=True, null=True)
    absorp = models.FloatField(u'Водопоглащение, %', blank=True, null=True)
    flex = models.IntegerField(u'Гибкость', blank=True, null=True)
    resist = models.CharField(u'Сопротивление статическому продавливанию', max_length=255, blank=True)
    comb = models.CharField(u'Группа горючести', max_length=255, blank=True)
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']