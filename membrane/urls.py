from django.conf.urls import patterns, url
from .views import MembraneDetail, CategoryDetail, BrandDetail


urlpatterns = patterns('',
                       url(r'^category/(?P<slug>[-_\w]+)/', CategoryDetail.as_view(), {}, 'category'),
                       url(r'^brand/(?P<slug>[-_\w]+)/', BrandDetail.as_view(), {}, 'brand'),
                       url(r'^(?P<slug>[-_\w]+)/', MembraneDetail.as_view(), {}, 'membrane'),
                       )

