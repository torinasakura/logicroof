from django.views.generic import DetailView
from .models import Membrane, Brand, MembraneCategory
from document.models import Document
from accessory.models import Accessory
from object.models import Object
from solution.models import Solution
from article.models import Article


class MembraneDetail(DetailView):
    model = Membrane
    context_object_name = 'item'
    template_name = 'membrane/index.html'

    def get_context_data(self, **kwargs):
        context = super(MembraneDetail, self).get_context_data(**kwargs)
        context['other_membranes'] = Membrane.objects.filter(brand=self.object.brand).exclude(id=self.object.id)[:10]
        context['membrane_docs'] = Document.objects.filter(membrane=self.object).exclude(category_id=4)
        context['membrane_promotionals'] = Document.objects.filter(membrane=self.object, category_id=4)
        context['membrane_solution'] = Solution.objects.filter(membrane=self.object)[:10]
        context['membrane_accesory'] = Accessory.objects.filter(membrane=self.object)[:10]
        context['objects'] = Object.objects.filter(membrane=self.object)[:10]
        return context


class CategoryDetail(DetailView):
    model = MembraneCategory
    context_object_name = 'item'
    template_name = 'membrane/type.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryDetail, self).get_context_data(**kwargs)
        membranes = self.object.membrane_set.all()
        context['accessories'] = Accessory.objects.filter(membrane__in=membranes).distinct()[:10]
        context['types_solutions'] = Solution.objects.filter(membrane__in=membranes).distinct()[:10]
        context['types_articles'] = Article.objects.filter(membrane__in=membranes).distinct()[:10]
        context['types_objects'] = Object.objects.filter(membrane__in=membranes).distinct()[:10]
        context['types_brands'] = Brand.objects.filter(membrane__in=membranes).distinct()[:10]
        return context


class BrandDetail(DetailView):
    model = Brand
    context_object_name = 'item'
    template_name = 'membrane/brand.html'

    def get_context_data(self, **kwargs):
        context = super(BrandDetail, self).get_context_data(**kwargs)
        membranes = self.object.membrane_set.all()
        context['brand_accessories'] = Accessory.objects.filter(membrane__in=membranes).distinct()[:10]
        context['brand_solutions'] = Solution.objects.filter(membrane__in=membranes).distinct()[:10]
        context['brand_objects'] = Object.objects.filter(membrane__in=membranes).distinct()[:10]
        context['brand_doc'] = Document.objects.filter(membrane__in=membranes).exclude(category_id=4).distinct()
        context['brand_materials'] = Document.objects.filter(membrane__in=membranes, category_id=4).distinct()
        return context