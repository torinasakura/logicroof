'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var bs = require('browser-sync');

//- --------- CONFIGS
var port = cfg.port;
var staticDirPath = cfg.staticDirPath;
var bs1 = require('browser-sync').create('nodejs');
var bs2 = require('browser-sync').create('django');

var bsCfg = function(port) {
  return {
    files: [staticDirPath + '/**/*.{html,css,js}'],
    proxy: {
      target: 'http://localhost:' + port
    },
    port: parseInt(port) + 1,
    ui: {
      port: parseInt(port) + 2
    },
    open: false,
    browser: 'google chrome',
    reloadOnRestart: true,
    notify: true,
    scrollProportionally: false,
    plugins: ['bs-html-injector'],
    ghostMode: {
      clicks: true,
      location: true,
      forms: true,
      scroll: true
    },
    injectChanges: true,
    logPrefix: 'BS',
    logConnections: true,
    logFileChanges: true,
    logSnippet: true,
    snippetOptions: {
      rule: {
        match: /<\/body>/i,
        fn: function(snippet, match) {
          return snippet + match;
        }
      }
    },
    minify: true,
    startPath: ''
  };
};

//- --------- TASKS
g.task('bs', ['bs2', 'nodemon'], function() {
  bs1.init(bsCfg(port));
});

g.task('bs2', function() {
  bs2.init(bsCfg(8000));
});