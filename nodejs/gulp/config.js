'use strict';

//- --------- REQUIREMENTS
var path = require('path');

//- --------- CONFIGS
var
  src = '',
  dest = '',
  styles = '/stylus',
  views = '/jade',
  img = '/img',
  icons = '/icons',
  fonts = '/fonts',
  js = '/js',
  indexjs = '/index.js',
  bower = '/libs',
  projectRoot = '..',
  staticDir = '/static_local',
  recurse = '/**',
  staticDirPath = path.join(projectRoot, staticDir),
  port = 3000
;

var dir = {
  views: '/jade',
  djangoTemplates: '/django'
};

module.exports = {
  port: port,
  projectRoot: projectRoot,
  staticDir: staticDir,
  staticDirPath: staticDirPath,
  src: {
    js: path.join(staticDirPath,src,js,'/src')
  },
  dest: {
    js: staticDirPath + dest + js
  },
  views: staticDirPath + src + views,
  jade: {
    src: [staticDirPath + src + views + '/views/*.jade', '!' + staticDirPath + src + views + '/**/_*{.jade,/**}'],
    mon: [staticDirPath + src + views + '/**/*.jade'],
    dest: staticDirPath + dest
  },
  styl: {
    src: [staticDirPath + src + styles + '/*.styl', '!' + staticDirPath + src + styles + '/**/_*{.styl,/**}'],
    mon: [staticDirPath + src + styles + '/**/*.styl'],
    dest: staticDirPath + dest + '/css'
  },
  iconfont: {
    src: staticDirPath + src + img + icons + '/*.svg',
    dest: staticDirPath + dest + fonts + icons,
    fontPath: dest + fonts + icons
  },
  coffee: {
    src: staticDirPath + src + js + '/*.coffee',
    dest: staticDirPath + dest + js
  },
  js: {
    //src: path.join(staticDirPath, src, js, recurse, '/*.js'),
    src: path.join('../static_local/js/src/**/*.js'),

    dest: path.join(staticDirPath, dest, js)
  },
  images: {
    src: path.join(staticDirPath, '/images', '/**/*.{png,jpg,jpeg,gif}'),
    dest: path.join(staticDirPath, dest, '/images')
  },
  bower: {
    dest: staticDirPath + dest + js + '/bower'
  },
  load: {
    DEBUG: false,
    pattern: ['gulp-*', 'gulp.*'],
    lazy: false,
    rename: {
      'gulp-changed': 'changed',
      'main-bower-files': 'mainBowerFiles',
      'autoprefixer-stylus': 'autoprefixer',
      'browserSync': 'browser-sync',
      'merge2': 'merge2'
    }
  },
  g: require('gulp'),
  $: require('gulp-load-plugins')({
      DEBUG: false,
      pattern: ['gulp-*', 'gulp.*'],
      lazy: false,
      config: './package.json',
      rename: {
        'gulp-changed': 'changed',
        'gulp-main-bower-files': 'mainBowerFiles',
        'autoprefixer-stylus': 'autoprefixer',
        'browserSync': 'browser-sync',
        'merge2': 'merge2'
      }
    })
};