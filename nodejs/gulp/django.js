'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var shell = require('gulp-shell');

//- --------- CONFIGS


//- --------- TASKS
g.task('django', shell.task(['/bin/bash -c ". ../.env/bin/activate; python ../manage.py runserver 0.0.0.0:8000"']));