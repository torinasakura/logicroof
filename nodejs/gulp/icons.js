'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');

var changed = require('gulp-changed');
var debug = require('gulp-debug');
var rename = require('gulp-rename');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var remove = require('gulp-html-remove');
var replace = require('gulp-replace');
var minifyHTML = require('gulp-minify-html');
var svgmin = require('gulp-svgmin');

//- --------- CONFIGS
var runTimestamp = Math.round(Date.now() / 1000);
var configs = {
  iconfont: {
    fontName: 'icons',
    normalize: true,
    fontHeight: 1000,
    appendUnicode: false,
    formats: ['eot', 'woff2', 'woff', 'svg', 'ttf'],
    timestamp: runTimestamp
  },
  rename: {
    dirname: ''
  },
  minifyHTML: {
    quotes: true
  },
  svgmin: {
    plugins: [
      {
        cleanupAttrs: {
          newlines: true,
          trim: true,
          spaces: true
        }
      }, {
        removeDoctype: true
      }, {
        removeXMLProcInst: true
      }, {
        removeComments: true
      }, {
        removeMetadata: true
      }, {
        removeTitle: true
      }, {
        removeDesc: {
          removeAny: false
        }
      }, {
        removeUselessDefs: true
      }, {
        removeEditorsNSData: {
          additionalNamespaces: ['<i:pgf>', '<i:pgfRef/>']
        }
      }, {
        removeEmptyAttrs: true
      }, {
        removeHiddenElems: {
          displayNone: true,
          opacity0: true,
          circleR0: true,
          ellipseRX0: true,
          ellipseRY0: true,
          rectWidth0: true,
          rectHeight0: true,
          patternWidth0: true,
          patternHeight0: true,
          imageWidth0: true,
          imageHeight0: true,
          pathEmptyD: true,
          polylineEmptyPoints: true,
          polygonEmptyPoints: true
        }
      }, {
        removeEmptyText: {
          text: true,
          tspan: true,
          tref: true
        }
      }, {
        removeEmptyContainers: true
      }, {
        removeViewBox: true
      }, {
        removeUnknownsAndDefaults: {
          unknownContent: true,
          unknownAttrs: true,
          defaultAttrs: true,
          uselessOverrides: true,
          keepDataAttrs: true
        }
      }, {
        removeNonInheritableGroupAttrs: true
      }, {
        removeUselessStrokeAndFill: {
          stroke: true,
          fill: true
        }
      }, {
        removeUnusedNS: true
      }, {
        removeRasterImages: true
      }, {
        removeDimensions: true
      }, {
        removeAttrs: {
          attrs: ['*:requiredExtensions']
        }
      }, {
        cleanupEnableBackground: true
      }, {
        cleanupNumericValues: {
          floatPrecision: 3,
          leadingZero: true,
          defaultPx: true,
          convertToPx: true
        }
      }, {
        cleanupIDs: {
          remove: false,
          minify: false,
          prefix: ''
        }
      }, {
        convertStyleToAttrs: false
      }, {
        convertColors: {
          names2hex: true,
          rgb2hex: true,
          shorthex: true,
          shortname: true
        }
      }, {
        convertPathData: {
          applyTransforms: true,
          applyTransformsStroked: true,
          straightCurves: true,
          lineShorthands: true,
          curveSmoothShorthands: true,
          floatPrecision: 3,
          transformPrecision: 5,
          removeUseless: true,
          collapseRepeated: true,
          utilizeAbsolute: true,
          leadingZero: true,
          negativeExtraSpace: true
        }
      }, {
        convertTransform: {
          convertToShorts: true,
          floatPrecision: 3,
          transformPrecision: 5,
          matrixToTransform: true,
          shortTranslate: true,
          shortScale: true,
          shortRotate: true,
          removeUseless: true,
          collapseIntoOne: true,
          leadingZero: true,
          negativeExtraSpace: false
        }
      }, {
        convertShapeToPath: true
      }, {
        moveElemsAttrsToGroup: true
      }, {
        moveGroupAttrsToElems: true
      }, {
        collapseGroups: true
      }, {
        mergePaths: {
          collapseRepeated: true,
          leadingZero: true,
          negativeExtraSpace: true
        }
      }, {
        sortAttrs: {
          order: ['xmlns', 'id', 'width', 'height', 'x', 'x1', 'x2', 'y', 'y1', 'y2', 'cx', 'cy', 'r', 'fill', 'fill-opacity', 'fill-rule', 'stroke', 'stroke-opacity', 'stroke-width', 'stroke-miterlimit', 'stroke-dashoffset', 'd', 'points']
        }
      }, {
        transformsWithOnePath: {
          width: false,
          height: false,
          scale: false,
          shiftX: false,
          shiftY: false,
          hcrop: false,
          vcenter: false,
          floatPrecision: 3,
          leadingZero: true,
          negativeExtraSpace: true
        }
      }, {
        addClassesToSVGElement: false
      }
    ],
    js2svg: {
      pretty: false
    }
  },
  changed: {
    extension: '.svg',
    hasChanged: changed.compareSha1Digest
  }
};

//- --------- TASKS

g.task('icons', function() {
  return g.src(cfg.iconfont.src)
    .pipe(debug({ title: 'iconfont-debug:' }))
    .pipe(changed( cfg.styl.dest, configs.changed))
    .pipe(rename(configs.rename))
    .pipe(remove('[id][id="adobe_illustrator_pgf"]'))
    .pipe(replace(/\s+xmlns:(x|i|graph|xlink)=\"[^\"]*\"/ig, ''))
    .pipe(replace(/<!ENTITY\s+[^>]*.>/ig, ''))
    .pipe(replace(/<!DOCTYPE\s+[^>]*.>/ig, ''))
    .pipe(replace(/<(\/)?switch[^>]*>/ig, ''))
    .pipe(replace(/\s+i:[^=]*="[^"]*"/ig, ''))
    .pipe(minifyHTML(configs.minifyHTML))
    .pipe(replace(/<foreignObject\s+.*(requiredExtensions)\s?=.*>.*<\/foreignObject>/ig, ''))
    .pipe(replace(/\sid="XMLID_[^"]*"/ig, ''))
    .pipe(svgmin(configs.svgmin))
    .pipe(iconfontCss({
      fontName: configs.iconfont.fontName,
      targetPath: '../../stylus' + '/_' + configs.iconfont.fontName + '.styl',
      fontPath: '/static' + cfg.iconfont.fontPath + '/',
      cssClass: 'icon'
    }))
    .pipe(iconfont(configs.iconfont))
    .pipe(g.dest(cfg.iconfont.dest));
});