'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var imageop = require('gulp-image-optimization');

//- --------- CONFIGS


//- --------- TASKS
g.task('images', function(cb) {
  g.src(cfg.images.src)
    .pipe(imageop({
      optimizationLevel: 7,
      progressive: true,
      interlaced: false
    }))
    .pipe(g.dest(cfg.images.dest))
    .on('end', cb).on('error', cb);
});