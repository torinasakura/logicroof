'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var debug = require('gulp-debug');
var rename = require('gulp-rename');
var jade = require('gulp-jade');
var jadeInheritance = require('gulp-jade-inheritance');

//- --------- CONFIGS


//- --------- TASKS
g.task('jade', function() {
  return g.src(cfg.jade.src)
    .pipe(debug({title: 'jade-debug:'}))
    .pipe(jadeInheritance({
      basedir: cfg.views + '/'
    }))
    .pipe(jade({
      jade: require('jade'),
      client: false,
      pretty: true,
      locals: {$django: true}
    }))
    .on('error', console.log)
    .pipe(rename({dirname: ''}))
    .pipe(g.dest(cfg.jade.dest));
});
