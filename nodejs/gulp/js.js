'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var merge2 = require('merge2');
var bower = require('gulp-main-bower-files');
var gutil = require('gulp-util');
var debug = require('gulp-debug');
var rename = require('gulp-rename');
var order = require('gulp-order');
var concat = require('gulp-concat');
var coffee = require('gulp-coffee');
var cache = require('gulp-cache-bust');
var uglify = require('gulp-uglify');

//- --------- CONFIGS
var orderCfg = [
  'headroom.js',
  'headroom.min.js',
  'jquery.js',
  'jquery.min.js',
  'modernizr.custom.js',
  'jquery.lazyload.js',
  'jQuery.headroom.js',
  'jQuery.headroom.min.js',
  'jquery.waypoints.js',
  'jquery.waypoints.min.js',
  'inview.min.js',
  'jquery.cookie.js',
  'select2.js',
  'nest.js',
  'scrollRoof.js',
  'warpless.js',
  'jquery.multilineellipsis.js',
  'wuniClient.js',
  'iframeResizer.min.js',
  'svgxuse.min.js'
];

var bowerCfg = {
  overrides: {
    waypoints: {
      ignore: false,
      main: [
        './lib/jquery.waypoints.min.js'
      ]
    },
    select2: {
      ignore: false,
      main: './dist/js/select2.js'
    },
    jquery_lazyload: {
      ignore: false,
      main: './jquery.lazyload.js'
    },
    jquery: {
      ignore: false,
      main: './dist/jquery.min.js'
    },
    'headroom.js': {
      //ignore: true,
      //main: './dist/jQuery.headroom.min.js'
      //main: './dist/jQuery.headroom.js'
      //main: './dist/headroom.js'
      main: './dist/headroom.min.js'
    },
    'iframe-resizer': {
      main: './js/iframeResizer.min.js'
    },
    'svgxuse': {
      main: './svgxuse.min.js'
    }
  }
};

//- --------- TASKS
g.task('js', function() {
  return merge2(
    g.src('./bower.json')
      .pipe(cache('js-bower'))
      .pipe(bower(bowerCfg)),
    g.src('../static_local/js/src/custom-libs/**/*.js')
      .pipe(cache('js-libs')),
    g.src('../static_local/js/src/inits.js')
      .pipe(cache('js-inits'))
  )
    .pipe(cache({type: 'js'}))
    .pipe(debug({title: 'js-debug:'})).on('error', gutil.log)
    .pipe(rename({dirname: ''}))
    .pipe(order(orderCfg))
    .pipe(concat('index.js'))
    .pipe(g.dest(cfg.js.dest))

    .pipe(uglify())
    .pipe(rename({extname: '.min.js'}))
    .pipe(g.dest(cfg.js.dest));
});