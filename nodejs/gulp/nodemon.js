'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var nodemon = require('gulp-nodemon');
var bs = require('browser-sync');
var path = require('path');

//- --------- CONFIGS
var port = cfg.port;
var delay = 500;
var nodemonCfg = {
  script: ['./server.js'],
  tasks: [],
  ext: 'js html css',
  env: {
    'NODE_ENV': 'development',
    'PORT': port
  },
  watch: [
    'server.js',
    'gulp/default.js',
    'gulp/nodemon.js',
    'gulp/bs.js',
    'gulp/jade.js',
    'gulp/styl.js',
    'gulp/watch.js',
    'gulp/config.js',
    'gulp/scripts.js'
  ],
  ignore: [
    '.git',
    'node_modules',
    'bower_components',
    'public',
    '.idea',
    'log'
  ]
};

//- --------- TASKS
g.task('nodemon', function(cb) {
  var called, started;
  called = false;
  started = false;
  return nodemon(nodemonCfg).on('start', function() {
    if (!called) {
      cb();
      started = true;
    }
    called = true;
    console.log('started!')
  }).on('restart', function() {
    setTimeout((function() {
      console.log('restarted!');
      bs.reload({
        stream: false
      });
    }), delay);
  });
});