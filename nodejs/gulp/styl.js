'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');
var changed = require('gulp-changed');
var sourcemaps = require('gulp-sourcemaps');
var stylus = require('gulp-stylus');
var px2rem = require('gulp-px2rem');
var debug = require('gulp-debug');
var pixrem = require('gulp-pixrem');
var autoprefixer = require('autoprefixer-stylus');
var combineMq = require('gulp-combine-mq');
var csscomb = require('gulp-csscomb');
var csso = require('gulp-csso');
var rename = require('gulp-rename');
var bs = require('browser-sync');
var cssnano = require('gulp-cssnano');
var uncss = require('gulp-uncss');
var path = require('path');
var cached = require('gulp-cached');
//var cache = require('gulp-cache');
var remember = require('gulp-remember');
var purge = require('gulp-css-purge');

//- --------- CONFIGS
var configs = {
  remify: {
    rootValue: 16,
    unitPrecision: 5,
    propertyBlackList: [],
    propertyWhiteList: [],
    replace: true,
    mediaQuery: false,
    minPx: 1
  },
  stylus: {
    'include css': true,
    use: [
      autoprefixer({
          browsers: [
            'last 5 Chrome versions',
            'last 5 Firefox versions',
            'last 3 Opera versions',
            'Safari >= 7.1',
            'iOS >= 7.1',
            'ie >= 10',
            'Edge >= 12',
            'Android >= 4.4',
            'BlackBerry >= 10',
            'OperaMobile >= 12.1',
            'OperaMini >= 8'
          ]
      }
      //  {
      //  //browsers: ['last 5 Chrome versions', 'Safari >= 8.02', 'ie >= 10']
      //  //browsers: ['last 5 Chrome versions', 'Safari >= 8.02', 'ie >= 10']
      //}
      )
    ]
  },
  changed: {
    extension: '.css',
    hasChanged: changed.compareSha1Digest
  },
  uncss: {
    html: (function(){
      var paths = [
        'accessory/',
        'article/',
        'document/',
        'index/',
        'membrane/',
        'object/',
        'solution/',
        'video/'
      ];
      //var js = path.join(cfg.src.js, '/**/*.js');
      //var html = paths.map(function(i){return path.join(cfg.projectRoot, i, 'templates/**/*.html')});
      //html.push(path.join(cfg.src.js, '/**/*.js'));
      return paths.map(function(i){return path.join(cfg.projectRoot, i, 'templates/**/*.html')});
    })(),
    ignore: [
      '.asideTrue',
      '.sidebar',
      '.open',
      '.asideToggle',
      '.ipnbarAnchorsItem',
      '.active',
      '.activeLast',
      '#ipnbar',
      '#topbar',
      '#ipnbar',
      '#main-preloader',
      '.js-sidebarContainerMenu',
      '.js-sidebarContainer',
      '.js-endpoint',
      '.js-ipnbarAnchorsItemLink',
      '.headroom--unpinned',
      '.headroom--pinned',
      '.headroom--pinned--force',
      '.ipnbarAnchors',
      '.waypoint-topbar',
      '.waypoint-collapsed',
      '.solutionPieCallout',
      '.heroImgCalloutNumber',
      '.heroImgCalloutText',
      '.selectField',
      '.catalog',
      '.previewImg',
      '.previewTitle',
      '.previewDesc',
      '.ipnbar-without-cta',
      '.ellipsisInner',
      '.nestCaret',
      '.nestText',
      '.roofScrollTrack',
      '.roofScrollThumb',
      '.warpless-list',
      '.warpless-track',
      '.warpless-slider',
      '.warpless-loading',
      '.warpless-initialized',
      '.warpless-vertical',
      '.warpless-arrow',
      '.dragging',
      '.warpless-hidden',
      '.carouselBar',
      '.carouselBarButtons',
      '.carouselBarButtonLeft',
      '.carouselBarButtonRight',
      '#category-select',
      '#membrane-select',
      '#solution-select',
      '#city-select'
    ]
  }
};

//- --------- TASKS
g.task('styl', function() {
  return g.src(cfg.styl.src)
    .pipe(sourcemaps.init())
    //.pipe(cached('styl', {optimizeMemory: true}))
    .pipe(remember('styl'))
    .pipe(stylus(configs.stylus)).on('error', console.log)
    //.pipe(purge())
    .pipe(cssnano())
    .pipe(csso())
    .pipe(combineMq({beautify: true}))
    .pipe(csscomb())
    .pipe(px2rem(configs.remify))
    //.pipe(uncss(configs.uncss))
    .pipe(sourcemaps.write())
    .pipe(g.dest(cfg.styl.dest))
    .pipe(bs.stream())

    // MINIFIED VERSION
    .pipe(csso())
    .pipe(rename({extname: '.min.css'}))
    .pipe(g.dest(cfg.styl.dest));
});