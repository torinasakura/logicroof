'use strict';

//- --------- REQUIREMENTS
var cfg = require('./config');
var g = require('gulp');

//- --------- CONFIGS


//- --------- TASKS
//gulp.task('isWatching', function() {
//  return global.isWatching = true;
//});

g.task('watch', function() {
  g.watch(cfg.styl.mon, ['styl']);
  //g.watch(cfg.jade.mon, ['jade']);
  g.watch(cfg.iconfont.mon, ['icons']);
  g.watch(cfg.js.src, ['js']);
});