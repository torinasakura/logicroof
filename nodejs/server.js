'use strict';
var express = require('express')
  , app = express()
  , path = require('path')
;

var
  projectRoot = '..'
  , staticDir = '/static_local'
  , staticDirPath = path.join(__dirname, projectRoot, staticDir)
;

var port = process.env.PORT || 3000;

app.use(express.static(staticDirPath));
app.use('/static', express.static(staticDirPath) );

//app.get('/', function(req, res) {res.sendFile(staticDirPath + '/main.html')});
app.get('/membrane', function(req, res) {res.sendFile(staticDirPath + '/membrane.html')});
app.get('/type', function(req, res) {res.sendFile(staticDirPath + '/type.html')});
app.get('/brand', function(req, res) {res.sendFile(staticDirPath + '/brand.html')});
app.get('/solution', function(req, res) {res.sendFile(staticDirPath + '/solution.html')});
app.get('/solutions', function(req, res) {res.sendFile(staticDirPath + '/solutions.html')});
app.get('/accessories', function(req, res) {res.sendFile(staticDirPath + '/accessories.html')});
app.get('/accessory', function(req, res) {res.sendFile(staticDirPath + '/accessory.html')});
app.get('/articles', function(req, res) {res.sendFile(staticDirPath + '/articles.html')});
app.get('/article', function(req, res) {res.sendFile(staticDirPath + '/article.html')});
app.get('/main', function(req, res) {res.sendFile(staticDirPath + '/main.html')});
app.get('/objects', function(req, res) {res.sendFile(staticDirPath + '/objects.html')});
app.get('/object', function(req, res) {res.sendFile(staticDirPath + '/object.html')});
app.get('/docs', function(req, res) {res.sendFile(staticDirPath + '/docs.html')});
app.get('/docsgroups', function(req, res) {res.sendFile(staticDirPath + '/docs-groups.html')});
app.get('/video', function(req, res) {res.sendFile(staticDirPath + '/video.html')});
app.get('/about', function(req, res) {res.sendFile(staticDirPath + '/about.html')});
app.get('/404', function(req, res) {res.sendFile(staticDirPath + '/404.html')});
app.get('/contacts', function(req, res) {res.sendFile(staticDirPath + '/contacts.html')});
app.get('/trainings', function(req, res) {res.sendFile(staticDirPath + '/trainings.html')});
app.get('/search', function(req, res) {res.sendFile(staticDirPath + '/search.html')});

app.listen(port);
