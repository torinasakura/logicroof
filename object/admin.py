from django.contrib import admin
from .models import ObjectCategory, Object, ObjectCity, ObjectCountry, ObjectType


admin.site.register(ObjectCategory)
admin.site.register(Object)
admin.site.register(ObjectCity)
admin.site.register(ObjectCountry)
admin.site.register(ObjectType)