# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):

    dependencies = [
        ('membrane', '0001_initial'),
        ('solution', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Object',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0431\u0440\u0435\u043d\u0434\u0430')),
                ('title_meta', models.CharField(max_length=255, verbose_name='')),
                ('slug', models.SlugField(verbose_name='URL')),
                ('img_main', models.ImageField(upload_to=b'object', verbose_name='\u0413\u043b\u0430\u0432\u043d\u043e\u0435 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('img_main_title', models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043f\u0440\u0435\u0432\u044c\u044e')),
                ('img_main_alt', models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f')),
                ('_desc_rendered', models.TextField(null=True, editable=False, blank=True)),
                ('desc', precise_bbcode.fields.BBCodeTextField(no_rendered_field=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f')),
                ('add', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='ObjectCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.CreateModel(
            name='ObjectCity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.CreateModel(
            name='ObjectCountry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.CreateModel(
            name='ObjectType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041f\u0440\u0435\u0438\u043c\u0443\u0449\u0435\u0441\u0442\u0432\u043e')),
            ],
        ),
        migrations.AddField(
            model_name='objectcity',
            name='country',
            field=models.ForeignKey(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', to='object.ObjectCountry'),
        ),
        migrations.AddField(
            model_name='object',
            name='category',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u043e\u0431\u044a\u0435\u043a\u0442\u0430', to='object.ObjectCategory'),
        ),
        migrations.AddField(
            model_name='object',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434\u0430', to='object.ObjectCity'),
        ),
        migrations.AddField(
            model_name='object',
            name='membrane',
            field=models.ManyToManyField(to='membrane.Membrane', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b'),
        ),
        migrations.AddField(
            model_name='object',
            name='solution',
            field=models.ManyToManyField(to='solution.Solution', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0432 \u043e\u0431\u044c\u0435\u043a\u0442\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='object',
            name='type',
            field=models.ForeignKey(verbose_name='', to='object.ObjectType'),
        ),
    ]
