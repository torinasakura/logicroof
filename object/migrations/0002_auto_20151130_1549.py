# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='object',
            name='title_meta',
            field=models.CharField(max_length=255, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u0442\u0435\u0433\u0430 <metatitle>'),
        ),
    ]
