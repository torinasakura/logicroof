# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0002_auto_20151130_1549'),
    ]

    operations = [
        migrations.AddField(
            model_name='object',
            name='area',
            field=models.IntegerField(default=0, verbose_name='\u041f\u043b\u043e\u0449\u0430\u0434\u044c'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='object',
            name='year',
            field=models.IntegerField(default=0, verbose_name='\u0413\u043e\u0434 \u043f\u043e\u0441\u0442\u0440\u043e\u0439\u043a\u0438'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='object',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='object.ObjectCity'),
        ),
        migrations.AlterField(
            model_name='object',
            name='membrane',
            field=models.ManyToManyField(to='membrane.Membrane', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u043c\u0435\u043c\u0431\u0440\u0430\u043d\u044b'),
        ),
        migrations.AlterField(
            model_name='object',
            name='solution',
            field=models.ManyToManyField(to='solution.Solution', verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u043c\u044b\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f'),
        ),
    ]
