# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0004_auto_20151202_1527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='object',
            name='type',
            field=models.ForeignKey(verbose_name='', blank=True, to='object.ObjectType', null=True),
        ),
    ]
