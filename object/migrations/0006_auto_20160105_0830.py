# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0005_auto_20151225_2318'),
    ]

    operations = [
        migrations.AddField(
            model_name='object',
            name='img_p',
            field=models.ImageField(upload_to=b'membrane', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043f\u0440\u0435\u0432\u044c\u044e', blank=True),
        ),
        migrations.AddField(
            model_name='object',
            name='img_p_alt',
            field=models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442', blank=True),
        ),
        migrations.AddField(
            model_name='object',
            name='img_p_title',
            field=models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442', blank=True),
        ),
    ]
