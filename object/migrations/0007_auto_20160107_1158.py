# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0006_auto_20160105_0830'),
    ]

    operations = [
        migrations.AddField(
            model_name='objecttype',
            name='order',
            field=models.IntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='objecttype',
            name='slug',
            field=models.SlugField(default=0, verbose_name='URL'),
            preserve_default=False,
        ),
    ]
