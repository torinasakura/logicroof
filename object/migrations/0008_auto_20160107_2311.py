# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0007_auto_20160107_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='object',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
        migrations.AlterField(
            model_name='objecttype',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
    ]
