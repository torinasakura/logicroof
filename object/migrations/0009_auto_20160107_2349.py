# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0008_auto_20160107_2311'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='object',
            options={'ordering': ['add']},
        ),
        migrations.AlterModelOptions(
            name='objecttype',
            options={'ordering': ['order']},
        ),
    ]
