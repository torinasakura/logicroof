# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0010_auto_20160107_2354'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='objectcategory',
            options={'ordering': ['order']},
        ),
        migrations.AddField(
            model_name='objectcategory',
            name='order',
            field=models.IntegerField(default=0, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='objectcategory',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
        migrations.AlterField(
            model_name='object',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u043e\u0431\u044a\u0435\u043a\u0442\u0430', to='object.ObjectCategory'),
        ),
    ]
