# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0011_auto_20160108_0058'),
    ]

    operations = [
        migrations.AlterField(
            model_name='object',
            name='desc',
            field=precise_bbcode.fields.BBCodeTextField(no_rendered_field=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043e\u0431\u044a\u0435\u043a\u0442\u0430'),
        ),
    ]
