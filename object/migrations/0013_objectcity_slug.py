# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0012_auto_20160109_1611'),
    ]

    operations = [
        migrations.AddField(
            model_name='objectcity',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
    ]
