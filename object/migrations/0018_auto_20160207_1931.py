# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0017_objectcategory_title_meta'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='object',
            options={'ordering': ['order']},
        ),
    ]
