# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('object', '0018_auto_20160207_1931'),
    ]

    operations = [
        migrations.AddField(
            model_name='objectcountry',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
    ]
