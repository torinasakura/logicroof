# -*- coding: utf-8 -*-
from django.db import models
from precise_bbcode.fields import BBCodeTextField
from membrane.models import Membrane
from solution.models import Solution
from slugify import slugify


class ObjectType(models.Model):
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class ObjectCategory(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class ObjectCountry(models.Model):
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u'URL', blank=True)

    def __unicode__(self):
        return self.title


class ObjectCity(models.Model):
    title = models.CharField(u'Название', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    country = models.ForeignKey(ObjectCountry, verbose_name=u'Страна')

    def save(self, **kwargs):
        slug = '%s' % self.title
        self.slug = slugify(slug)
        super(ObjectCity, self).save()

    def __unicode__(self):
        return '%s - %s' % (self.title, self.country)


class Object(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    img_p = models.ImageField(u'Изображение превью', upload_to='membrane', blank=True)
    img_p_title = models.CharField(u'Title-атрибут', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут', max_length=255, blank=True)
    img_main = models.ImageField(u'Главное изображение', upload_to='object', blank=True)
    img_main_title = models.CharField(u'Title-атрибут изображения превью', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут изображения', max_length=255, blank=True)
    type = models.ForeignKey(ObjectType, verbose_name=u'', null=True, blank=True)
    category = models.ForeignKey(ObjectCategory, verbose_name=u'Категория объекта')
    city = models.ForeignKey(ObjectCity, verbose_name=u'Город')
    year = models.IntegerField(u'Год постройки')
    area = models.IntegerField(u'Площадь')
    desc = BBCodeTextField(u'Описание объекта')
    membrane = models.ManyToManyField(Membrane, verbose_name=u'Используемые мембраны', blank=True)
    solution = models.ManyToManyField(Solution, verbose_name=u'Используемые решения', blank=True)
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']