from django.conf.urls import patterns, url
from .views import ObjectDetail, ObjectCatalogView


urlpatterns = [
    url(r'^catalog/$', ObjectCatalogView.as_view(), {}, 'object_category'),
    url(r'^catalog/(?P<cat>[-_\w]+)/(?P<m>[-_\w]+)/(?P<s>[-_\w]+)/(?P<co>[-_\w]+)/(?P<c>[-_\w]+)/$',
        ObjectCatalogView.as_view(), {}, 'object_category'),
    url(r'^ll/(?P<cat>[-_\w]+)/(?P<m>[-_\w]+)/(?P<s>[-_\w]+)/(?P<co>[-_\w]+)/(?P<c>[-_\w]+)/(?P<o>[0-9]+)/(?P<l>[0-9]+)/$',
        ObjectCatalogView.as_view(template_name='object/ll.html'), {}, 'object_category_ll'),
    url(r'^rawcatalog/(?P<cat>[-_\w]+)/(?P<m>[-_\w]+)/(?P<s>[-_\w]+)/(?P<co>[-_\w]+)/(?P<c>[-_\w]+)/',
        ObjectCatalogView.as_view(template_name='object/rawcatalog.html'), {}, 'object_category'),
    url(r'^(?P<slug>[-_\w]+)/', ObjectDetail.as_view(), {}, 'object'),
]
