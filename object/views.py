from django.views.generic import DetailView, View
from .models import Object, ObjectCategory, ObjectCity, ObjectCountry
from membrane.models import Membrane
from solution.models import Solution
from django.shortcuts import render


class ObjectDetail(DetailView):
    model = Object
    context_object_name = 'item'
    template_name = 'object/index.html'

    def get_context_data(self, **kwargs):
        context = super(ObjectDetail, self).get_context_data(**kwargs)
        context['other_objects'] = Object.objects.filter(category=self.object.category).exclude(id=self.object.id)
        return context


class ObjectCatalogView(View):
    template_name = 'object/catalog.html'

    def get(self, request, cat='all', m='all', s='all', co='all', c='all', l=12, o=0):
        context = dict()
        l = int(l)
        o = int(o)
        objects = Object.objects.filter()
        if cat != 'all':
            objects = objects.filter(category__slug=cat)
            context['category_title'] = ObjectCategory.objects.filter(slug=cat)
        if m != 'all':
            objects = objects.filter(membrane__slug=m)
        if s != 'all':
            objects = objects.filter(solution__slug=s)
        if c != 'all':
            objects = objects.filter(city__id=c)
        if co != 'all':
            city = ObjectCity.objects.filter(country__id=co)
            objects = objects.filter(city__in=city)
        context['objects'] = objects[o:o + l]
        context['object_category'] = ObjectCategory.objects.filter(object__set_in=context['objects']).distinct()
        context['object_category_all'] = ObjectCategory.objects.filter()
        context['object_membranes'] = Membrane.objects.filter(object__set_in=context['objects']).distinct()
        context['object_membranes_all'] = Membrane.objects.filter()
        context['object_solution'] = Solution.objects.filter(object__set_in=context['objects']).distinct()
        context['object_solution_all'] = Solution.objects.filter()
        context['object_city'] = ObjectCity.objects.filter(object__set_in=context['objects']).distinct()
        context['object_city_all'] = ObjectCity.objects.filter()
        context['object_country'] = ObjectCountry.objects.filter(objectcity__set_in=context['object_city']).distinct()
        context['object_country_all'] = ObjectCountry.objects.filter()
        context['js'] = {'cat': cat, 'm': m, 's': s, 'co': co, 'c': c, }
        return render(request, self.template_name, context)