from django.contrib import admin
from .models import SolutionAdv, SolutionCategory, Solution


admin.site.register(SolutionAdv)
admin.site.register(SolutionCategory)
admin.site.register(Solution)