# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0003_auto_20151202_1527'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solution',
            old_name='img_main',
            new_name='img_p',
        ),
        migrations.RenameField(
            model_name='solution',
            old_name='img_main_alt',
            new_name='img_p_alt',
        ),
        migrations.RenameField(
            model_name='solution',
            old_name='img_main_title',
            new_name='img_p_title',
        ),
    ]
