# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0004_auto_20151229_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='solutioncategory',
            name='desc',
            field=models.TextField(default='', verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f'),
            preserve_default=False,
        ),
    ]
