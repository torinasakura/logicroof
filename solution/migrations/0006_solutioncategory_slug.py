# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0005_solutioncategory_desc'),
    ]

    operations = [
        migrations.AddField(
            model_name='solutioncategory',
            name='slug',
            field=models.SlugField(default='', verbose_name='URL'),
            preserve_default=False,
        ),
    ]
