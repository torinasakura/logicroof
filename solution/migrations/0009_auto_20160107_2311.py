# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0008_auto_20160107_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solution',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
        migrations.AlterField(
            model_name='solutioncategory',
            name='slug',
            field=models.SlugField(verbose_name='URL', blank=True),
        ),
    ]
