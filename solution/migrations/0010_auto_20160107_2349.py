# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0009_auto_20160107_2311'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='solution',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='solutioncategory',
            options={'ordering': ['order']},
        ),
    ]
