# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('solution', '0011_auto_20160107_2354'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solution',
            old_name='img_main_ext',
            new_name='img_main',
        ),
        migrations.RenameField(
            model_name='solution',
            old_name='img_main_ext_alt',
            new_name='img_main_alt',
        ),
        migrations.RenameField(
            model_name='solution',
            old_name='img_main_ext_title',
            new_name='img_main_title',
        ),
    ]
