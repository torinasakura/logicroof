# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0013_auto_20160111_1016'),
    ]

    operations = [
        migrations.AddField(
            model_name='solution',
            name='svg',
            field=models.TextField(default=' ', verbose_name='SVG \u0441\u043e\u0440\u0435\u0446'),
            preserve_default=False,
        ),
    ]
