# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0015_solution_title_meta'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solution',
            name='img_main_alt',
            field=models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0433\u043b\u0430\u0432\u043d\u043e\u0433\u043e \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='solution',
            name='img_main_title',
            field=models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u0433\u043b\u0430\u0432\u043d\u043e\u0433\u043e \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='solution',
            name='img_p',
            field=models.ImageField(upload_to=b'solution', verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e-\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='solution',
            name='img_p_alt',
            field=models.CharField(max_length=255, verbose_name='Alt-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u043f\u0440\u0435\u0432\u044c\u044e-\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='solution',
            name='img_p_title',
            field=models.CharField(max_length=255, verbose_name='Title-\u0430\u0442\u0440\u0438\u0431\u0443\u0442 \u043f\u0440\u0435\u0432\u044c\u044e-\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f', blank=True),
        ),
        migrations.AlterField(
            model_name='solution',
            name='svg',
            field=models.TextField(verbose_name='SVG-\u043a\u044d\u043b\u043b\u0430\u0443\u0442\u044b'),
        ),
        migrations.AlterField(
            model_name='solution',
            name='titletext',
            field=models.TextField(verbose_name='\u0411\u0430\u0437\u043e\u0432\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0434\u043b\u044f \u043f\u0440\u0435\u0432\u044c\u044e'),
        ),
        migrations.AlterField(
            model_name='solution',
            name='titletext_ext',
            field=models.TextField(verbose_name='\u0420\u0430\u0441\u0448\u0438\u0440\u0435\u043d\u043d\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0434\u043b\u044f \u043a\u0430\u0442\u0430\u043b\u043e\u0433\u0430'),
        ),
    ]
