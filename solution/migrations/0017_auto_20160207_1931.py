# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import precise_bbcode.fields


class Migration(migrations.Migration):

    dependencies = [
        ('solution', '0016_auto_20160204_0547'),
    ]

    operations = [
        migrations.RenameField(
            model_name='solution',
            old_name='_desc_rendered',
            new_name='_body_rendered',
        ),
        migrations.RenameField(
            model_name='solution',
            old_name='titletext_ext',
            new_name='desc_ext',
        ),
        migrations.RemoveField(
            model_name='solution',
            name='titletext',
        ),
        migrations.AddField(
            model_name='solution',
            name='body',
            field=precise_bbcode.fields.BBCodeTextField(default='', no_rendered_field=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0440\u0435\u0448\u0435\u043d\u0438\u044f'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='solution',
            name='desc',
            field=models.TextField(verbose_name='\u0411\u0430\u0437\u043e\u0432\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0434\u043b\u044f \u043f\u0440\u0435\u0432\u044c\u044e'),
        ),
    ]
