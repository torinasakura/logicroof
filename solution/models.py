# -*- coding: utf-8 -*-
from django.db import models
from precise_bbcode.fields import BBCodeTextField
from membrane.models import Membrane


class SolutionCategory(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    desc = models.TextField(u'Описание решения')

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']


class SolutionAdv(models.Model):
    title = models.CharField(u'Название', max_length=255)

    def __unicode__(self):
        return self.title


class Solution(models.Model):
    title = models.CharField(u'Название', max_length=255)
    title_meta = models.CharField(u'Значение тега <metatitle>', max_length=255)
    slug = models.SlugField(u'URL', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    img_p = models.ImageField(u'Превью-изображение', upload_to='solution', blank=True)
    img_p_title = models.CharField(u'Title-атрибут превью-изображения', max_length=255, blank=True)
    img_p_alt = models.CharField(u'Alt-атрибут превью-изображения', max_length=255, blank=True)
    img_main = models.ImageField(u'Главное изображение', upload_to='solution', blank=True)
    img_main_title = models.CharField(u'Title-атрибут главного изображения', max_length=255, blank=True)
    img_main_alt = models.CharField(u'Alt-атрибут главного изображения', max_length=255, blank=True)
    category = models.ForeignKey(SolutionCategory, verbose_name=u'Тип гидроизоляции')
    body = BBCodeTextField(u'Описание решения')
    desc = models.TextField(u'Базовое описание для превью')
    desc_ext = models.TextField(u'Расширенное описание для каталога')
    svg = models.TextField(u'SVG-кэллауты')
    adv = models.ManyToManyField(SolutionAdv, verbose_name=u'Преимущества')
    membrane = models.ManyToManyField(Membrane, verbose_name=u'Используемые мембраны')
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']