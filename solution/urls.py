from django.conf.urls import patterns, url
from .views import SolutionDetail, SolutionCategoryView


urlpatterns = patterns('',
                       url(r'^category/(?P<slug>[-_\w]+)/$', SolutionCategoryView.as_view(), {}, 'solution_category'),
                       url(r'^(?P<slug>[-_\w]+)/$', SolutionDetail.as_view(), {}, 'solution'),
)
