from django.views.generic import DetailView
from .models import Solution, SolutionCategory
from accessory.models import Accessory
from object.models import Object
from document.models import Document


class SolutionDetail(DetailView):
    model = Solution
    context_object_name = 'item'
    template_name = 'solution/index.html'

    def get_context_data(self, **kwargs):
        context = super(SolutionDetail, self).get_context_data(**kwargs)
        membranes = self.object.membrane.all()
        context['accessories'] = Accessory.objects.filter(membrane__in=membranes)[:10]
        context['objects'] = self.object.object_set.filter()[:10]
        context['other_solutions'] = Solution.objects.filter(category=self.object.category).exclude(id=self.object.id)[
                                     :10]
        context['solution_docs'] = self.object.document_set.exclude(category_id=4)
        context['solution_promotionals'] = self.object.document_set.filter(category_id=4)
        return context


class SolutionCategoryView(DetailView):
    model = SolutionCategory
    context_object_name = 'item'
    template_name = 'solution/category.html'

    def get_context_data(self, **kwargs):
        context = super(SolutionCategoryView, self).get_context_data(**kwargs)
        return context