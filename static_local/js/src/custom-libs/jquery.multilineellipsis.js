/**
 * Created by Andrew Ghostuhin on 12.11.15.
 */
$.fn.multiLineEllipsis = function() {
    /*
     * Для блока к которому применяется данный плагин, необходимо обязательно указывать max-height
     */
    var _this = this;
    var $this = $(this);
    var length = $this.length;

    if (length == 1) {
        var $titleBlock = $this;
        var $innerTitleBlock;
        var maxHeight;
        var titleBlockText = $titleBlock.html();

        $titleBlock.text('');
        $innerTitleBlock = $('<div class="ellipsisInner">' + titleBlockText + '</div>').appendTo($titleBlock);

        if ($titleBlock.css('max-height') != 'none') {
            maxHeight = parseInt($titleBlock.css('max-height').replace('px', ''));
        } else {
            console.warn('multiLineEllipsis: warning! You haven\'t set max-height property. Plugin can work incorrect.');
            maxHeight = $titleBlock.height();
        }

        if ($innerTitleBlock.height() < maxHeight) {
            return false;
        } else {
            while ($innerTitleBlock.height() > maxHeight) {
                $innerTitleBlock.html(function(index, html) {
                    return html.replace(/[\s\.,:]*[^\s]+[\s]*$/, '...');
                });
            }

            return true;
        }
    } else if (length > 1) {
        _this.each(function() {
            var $titleBlock = $(this);
            var $innerTitleBlock;
            var maxHeight;
            var titleBlockText = $titleBlock.html();

            $titleBlock.text('');
            $innerTitleBlock = $('<div class="ellipsisInner">' + titleBlockText + '</div>').appendTo($titleBlock);

            if ($titleBlock.css('max-height') != 'none') {
                maxHeight = parseInt($titleBlock.css('max-height').replace('px', ''));
            } else {
                console.warn('multiLineEllipsis: warning! You haven\'t set max-height property. Plugin can work incorrect.');
                maxHeight = $titleBlock.height();
            }

            while ($innerTitleBlock.height() > maxHeight) {
                $innerTitleBlock.html(function(index, html) {
                    return html.replace(/[\s\.,:]*[^\s]+[\s]*$/, '...');
                });
            }
        });
    }
};
