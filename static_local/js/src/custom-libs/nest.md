
----------

#### caretHtml
  * **Type:** `string`
  * **Default:** ``

Raw html to inserted in the caret markup of the parent links:
`<a href="#link">Item<span>{:caretHtml}</span></a>`

#### accordion
  * **Type:** `boolean`
  * **Default:** `false`

Enable accordion mode.

----------

#### openClass:
  * **Type:** `string`
  * **Default:** `open`

CSS class to be added in open parent li.

----------

#### save:
  * **Type:** `boolean`
  * **Default:** `true`

Preserve expanded sub-menus between session. If jquery.cookie is not included it will be automatically turned off.

----------

#### cookie:
  * **Type:** `object`
    * `name`: Cookie name
      * *Type:* `string`
      * *Default:* `nest`
    * `expires`: Lifespan in days, `false` makes it a session cookie
      * *Type:* `integer|false`
      * *Default:* `false`
    * `path`: Path where cookie is valid
      * *Type:* `string`
      * *Default:* `/`

----------

#### slide:
  * **Type:** `object`
    * `duration`: Slide duration in milliseconds
      * *Type:* `integer`
      * *Default:* `400`
    * `easing`:	Slide easing function (linear|swing) for the transition
      * *Type:* `string`
      * *Default:* `swing`


## Callbacks
With the options you can also pass callback functions to extend the plugin's functionality.

#### onClickBefore:
This callback is executed before the plugin's main procedure when clicking links.
 * **Parameters**
   * `Event`: `Event Object`
   * `Submenu`: False if the clicked link is a leaf or the next `sub-menu` if link is a branch.

----------

#### onClickAfter:
This callback is executed after the plugin's main procedure when clicking links.
 * **Parameters**
   * `Event`: `Event Object`
   * `Submenu`: `False` if the clicked link is a leaf or the next `sub-menu` if link is a branch.

----------

#### onToggleBefore:
This callback is executed before the plugin's main procedure when toggling sub-menus.
 * **Parameters**
   * `Submenu`: `JQuery Object`
   * `Opening`: `True|False` the submenu is opening or closing

----------

#### onToggleAfter:
This callback is executed after the plugin's main procedure when toggling sub-menus.
 * **Parameters**
   * *Submenu:* `JQuery Object`
   * *Opened:* `True|False` the submenu opened or closed

## Public Methods

#### toggle
Manually open or close sub-menus.

  * Parameters:
    * `boolean`: Show or hide
    * `Variable list of indexes`: If omitted toggles **all sub-menus**
      *  `integer`
      *  `...`
      *  `integer`

```js
// Show|Hide all sub-menus
$(selector).nest('toggle', true|false);
```

```js
// Show|Hide sub-menus with specific indexes
// It will also open parent sub-menus since v0.1.2
$(selector).nest('toggle', true|false, 1, 2, ...);
```

----------

#### destroy
Destroy instances and unbind events.

```js
// I can't think of any other use except for testing...
$(selector).nest('destroy');
```
