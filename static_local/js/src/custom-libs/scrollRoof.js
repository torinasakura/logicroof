(function($) {
  $.fn.extend({
    roofScroll: function(options) {
      var defaults, o;
      defaults = {
        width: '100%',
        height: '100%',
        size: '3px',
        color: '#000',
        position: 'right',
        distance: '0px',
        start: 'top',
        opacity: 1,
        alwaysVisible: false,
        disableFadeOut: false,
        railVisible: false,
        railColor: '#333',
        railOpacity: .2,
        railDraggable: true,
        trackClass: 'js-roof-scroll-track',
        thumbClass: 'js-roof-scroll-thumb',
        barClass: 'js-roof-scroll-bar',
        wrapperClass: 'js-roof-scroll-div',
        wrapperHTML: '<div></div>',
        allowPageScroll: false,
        wheelStep: 16,
        touchScrollStep: 200,
        borderRadius: '0px',
        railBorderRadius: '0'
      };
      o = $.extend(defaults, options);
      this.each(function() {
        var _onWheel, wrapperHTML, attachWheel, bar, barHeight, divS, getBarHeight, height, hideBar, isDragg, isOverBar, isOverPanel, lastScroll, maxBarHeight, me, minBarHeight, offset, percentScroll, posCss, queueHide, releaseScroll, scrollContent, showBar, thumb, touchDif, track, wrapper;
        isOverPanel = void 0;
        isOverBar = void 0;
        isDragg = void 0;
        queueHide = void 0;
        touchDif = void 0;
        barHeight = void 0;
        percentScroll = void 0;
        lastScroll = void 0;
        divS = '<div></div>';
        wrapperHTML = o.wrapperHTML;
        minBarHeight = 30;
        maxBarHeight = 288;
        releaseScroll = false;
        me = $(this);
        _onWheel = function(e) {
          var e;
          var delta, target;
          if (!isOverPanel) {
            return;
          }
          e = e || window.event;
          delta = 0;
          if (e.wheelDelta) {
            delta = -e.wheelDelta / 120;
          }
          if (e.detail) {
            delta = e.detail / 3;
          }
          target = e.target || e.srcTarget || e.srcElement;
          if ($(target).closest('.' + o.wrapperClass).is(me.parent())) {
            scrollContent(delta, true);
          }
          if (e.preventDefault && !releaseScroll) {
            e.preventDefault();
          }
          if (!releaseScroll) {
            e.returnValue = false;
          }
        };
        scrollContent = function(y, isWheel, isJump) {
          var delta, maxTop, offsetTop;
          releaseScroll = false;
          delta = y;
          maxTop = me.outerHeight() - thumb.outerHeight();
          if (isWheel) {
            delta = parseInt(thumb.css('top')) + y * parseInt(o.wheelStep) / 100 * thumb.outerHeight();
            delta = Math.min(Math.max(delta, 0), maxTop);
            delta = y > 0 ? Math.ceil(delta) : Math.floor(delta);
            thumb.css({
              top: delta + 'px'
            });
          }
          percentScroll = parseInt(thumb.css('top')) / (me.outerHeight() - thumb.outerHeight());
          delta = percentScroll * (me[0].scrollHeight - me.outerHeight());
          if (isJump) {
            delta = y;
            offsetTop = delta / me[0].scrollHeight * me.outerHeight();
            offsetTop = Math.min(Math.max(offsetTop, 0), maxTop);
            thumb.css({
              top: offsetTop + 'px'
            });
          }
          me.scrollTop(delta);
          me.trigger('roofscrolling', ~~delta);
          showBar();
          hideBar();
        };
        attachWheel = function(target) {
          if (window.addEventListener) {
            target.addEventListener('DOMMouseScroll', _onWheel, false);
            target.addEventListener('mousewheel', _onWheel, false);
          } else {
            document.attachEvent('onmousewheel', _onWheel);
          }
        };
        getBarHeight = function() {
          var display;
          barHeight = Math.max(me.outerHeight() / me[0].scrollHeight * me.outerHeight(), minBarHeight);
          thumb.css({
            height: barHeight + 'px'
          });
          display = barHeight === me.outerHeight() ? 'none' : 'block';
          thumb.css({
            display: display
          });
        };
        showBar = function() {
          var msg;
          getBarHeight();
          clearTimeout(queueHide);
          if (percentScroll === ~~percentScroll) {
            releaseScroll = o.allowPageScroll;
            if (lastScroll !== percentScroll) {
              msg = ~~percentScroll === 0 ? 'top' : 'bottom';
              me.trigger('roofscroll', msg);
            }
          } else {
            releaseScroll = false;
          }
          lastScroll = percentScroll;
          if (barHeight >= me.outerHeight()) {
            releaseScroll = true;
            return;
          }
          thumb.stop(true, true).fadeIn('fast');
          if (o.railVisible) {
            track.stop(true, true).fadeIn('fast');
          }
        };
        hideBar = function() {
          if (!o.alwaysVisible) {
            queueHide = setTimeout((function() {
              if (!(o.disableFadeOut && isOverPanel) && !isOverBar && !isDragg) {
                thumb.fadeOut('slow');
                track.fadeOut('slow');
              }
            }), 1000);
          }
        };
        if (me.parent().hasClass(o.wrapperClass)) {
          offset = me.scrollTop();
          thumb = me.closest('.' + o.thumbClass);
          track = me.closest('.' + o.trackClass);
          bar = me.closest('.' + o.barClass);
          getBarHeight();
          if ($.isPlainObject(options)) {
            if ('height' in options && options.height === 'auto') {
              me.parent().css('height', 'auto');
              me.css('height', 'auto');
              height = me.parent().parent().height();
              me.parent().css('height', height);
              me.css('height', height);
            }
            if ('scrollTo' in options) {
              offset = parseInt(o.scrollTo);
            } else if ('scrollBy' in options) {
              offset += parseInt(o.scrollBy);
            } else if ('destroy' in options) {
              thumb.remove();
              track.remove();
              bar.remove();
              me.unwrap();
              return;
            }
            scrollContent(offset, false, true);
          }
          return;
        } else if ($.isPlainObject(options)) {
          if ('destroy' in options) {
            return;
          }
        }
        o.height = o.height === 'auto' ? me.parent().height() : o.height;
        wrapper = $(wrapperHTML).addClass(o.wrapperClass).css({
          position: 'relative',
          overflow: 'hidden'
        });
        me.css({
          overflow: 'hidden',
          height: o.height
        });
        bar = $(divS).addClass(o.barClass).css({
          position: 'fixed',
          width: '18rem',
          height: '100%'
        });
        track = $(divS).addClass(o.trackClass).css({
          width: o.size,
          height: '100%',
          position: 'absolute',
          top: 0,
          display: o.alwaysVisible && o.railVisible ? 'block' : 'none',
          'border-radius': o.railBorderRadius,
          background: o.railColor,
          zIndex: 90
        });
        thumb = $(divS).addClass(o.thumbClass).css({
          width: o.size,
          position: 'absolute',
          top: 0,
          opacity: o.opacity,
          display: o.alwaysVisible ? 'block' : 'none',
          'border-radius': o.borderRadius,
          BorderRadius: o.borderRadius,
          MozBorderRadius: o.borderRadius,
          WebkitBorderRadius: o.borderRadius,
          zIndex: 99
        });
        posCss = o.position === 'right' ? {
          right: o.distance
        } : {
          left: o.distance
        };
        bar.css;
        track.css(posCss);
        thumb.css(posCss);
        me.wrap(wrapper);
        me.parent().prepend(bar);
        bar.append(track);
        bar.append(thumb);
        if (o.railDraggable) {
          thumb.bind('mousedown', function(e) {
            var $doc, pageY, t;
            $doc = $(document);
            isDragg = true;
            t = parseFloat(thumb.css('top'));
            pageY = e.pageY;
            $doc.bind('mousemove.roofscroll', function(e) {
              var currTop;
              currTop = t + e.pageY - pageY;
              thumb.css('top', currTop);
              scrollContent(0, thumb.position().top, false);
            });
            $doc.bind('mouseup.roofscroll', function(e) {
              isDragg = false;
              hideBar();
              $doc.unbind('.roofscroll');
            });
            return false;
          }).bind('selectstart.roofscroll', function(e) {
            e.stopPropagation();
            e.preventDefault();
            return false;
          });
        }
        track.hover((function() {
          showBar();
        }), function() {
          hideBar();
        });
        thumb.hover((function() {
          isOverBar = true;
        }), function() {
          isOverBar = false;
        });
        me.hover((function() {
          isOverPanel = true;
          showBar();
          hideBar();
        }), function() {
          isOverPanel = false;
          hideBar();
        });
        me.bind('touchstart', function(e, b) {
          if (e.originalEvent.touches.length) {
            touchDif = e.originalEvent.touches[0].pageY;
          }
        });
        me.bind('touchmove', function(e) {
          var diff;
          if (!releaseScroll) {
            e.originalEvent.preventDefault();
          }
          if (e.originalEvent.touches.length) {
            diff = (touchDif - e.originalEvent.touches[0].pageY) / o.touchScrollStep;
            scrollContent(diff, true);
            touchDif = e.originalEvent.touches[0].pageY;
          }
        });
        getBarHeight();
        if (o.start === 'bottom') {
          thumb.css({
            top: me.outerHeight() - thumb.outerHeight()
          });
          scrollContent(0, true);
        } else if (o.start !== 'top') {
          scrollContent($(o.start).position().top, null, true);
          if (!o.alwaysVisible) {
            thumb.hide();
          }
        }
        attachWheel(this);
      });
      return this;
    }
  });
  $.fn.extend({
    roofscroll: $.fn.roofScroll
  });
})(jQuery);