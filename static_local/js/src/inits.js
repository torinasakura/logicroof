$debugToggle && $timeLog('Index.js evaluate start');

var $html = $('html');
var $body = $('body');
var $topbar = $('#topbar');
var $ipnbar = $('#ipnbar');
var $mainContainer = $('.js-mainContainer');
var $bars = $('#topbar, #ipnbar');
var $asideTrueClass = 'asideTrue';
var $locationPathname =  window.location.pathname;
var $asideToggle = $('.asideToggle');
var $ipnbarAnchorsItem = $( '.ipnbarAnchorsItem' );
var $classNameActive = 'active';
var $classNameActiveLast = 'activeLast';
var $fontSizeFactor = function(){return parseFloat($('html').css('font-size'))/16};
var $mainPreloader = $('#main-preloader');
var $sidebarContainerMenu = $('js-sidebarContainerMenu');
var $sidebarContainer = $('.js-sidebarContainer');


function zoom(debug){
  var $iw = window.innerWidth;
  var $cw = document.documentElement.clientWidth;
  var $fs = (parseFloat($html.css('font-size')) / 16 * 100).toFixed(3);
  function zoom(a){
    var $a = parseFloat(a).toFixed(3);
    if($a !== $fs){
      $html.css('font-size', $a + '%');
      if(debug === true){
        console.log('Zoom Debug | font size: ' + $a + '%' + ', inner width: ' + $iw + ', client width: ' + $cw);
      }
    }
  }
  function setStateClass(idx){
    var $classNames = ['s1', 's2', 's3', 's4'];
    $body.removeClass($classNames.join(' ')).addClass($classNames[idx -= 1]);
  }
  if($iw < 288){
    zoom(100);
    setStateClass(1);
  }
  if($iw >= 288  && 576 > $iw){
    zoom($cw / 2.88);
    setStateClass(1);
  }
  if($iw >= 576  && 864 > $iw){
    zoom($cw / 5.76);
    setStateClass(2);
    //$html.removeClass($asideTrueClass);
  }
  if($iw >= 864  && 1152 > $iw){
    zoom($cw / 8.64);
    setStateClass(3);
  }
  if($iw >= 1152 && 1440 > $iw){
    zoom($cw / 11.52);
    setStateClass(4);
    //$html.toggleClass($asideTrueClass);
  }
  if($iw >= 1440){
    zoom(125);
    setStateClass(4);
    //$html.toggleClass($asideTrueClass);
  }
}

$(window).on('load resize', function(){zoom()});
$body.one('DOMSubtreeModified', function(){
  zoom();
});

//$mainContainer.roofScroll({
//  wrapperHTML: '<main id="main" class="wrapper"></main>',
//  wrapperClass: 'js-content',
//  alwaysVisible: true,
//  railVisible: false,
//  width: '100%',
//  size: '12px'
//});

//$('#ipnbar .ipnbarAnchors').roofScroll({
//  //wrapperHTML: '<nav id="ipnbar" class="row ipnbar"></nav>',
//  wrapperClass: 'js-ipnbarAnchors',
//  alwaysVisible: true,
//  railVisible: false,
//  width: '100%',
//  position: 'bottom',
//  size: '12px'
//});

active_menu_cb = function(e, submenu) {
  e.preventDefault();
  $sidebarContainer.find('li').removeClass('active');
  var li =  $(this).parent();
  var lis = li.parents('li');
  li.addClass('active');
  lis.addClass('active');
};
$sidebarContainer
  .roofScroll({
    wrapperHTML: '<aside id="aside"></aside>',
    wrapperClass: 'sidebar',
    alwaysVisible: true,
    railVisible: false,
    size: '12px'
  })
  .nest({
    caretHtml: '',
    onClickAfter: active_menu_cb,
    accordion: true,
    openClass: 'open',
    toggle: true,
    save: true,
    cookie: {
      name: 'nest1',
      expires: false,
      path: '/'
    },
    slide: {
      duration: 400,
      easing: 'swing'
    }
  });

zoom();
$mainPreloader.fadeOut(300, 'swing').queue(function(){
  $body.css('overflow', 'inherit');
  zoom();
});

$asideToggle.click(function() {
  $html.toggleClass($asideTrueClass);
  zoom();//
});

$(".js-sidebarContainer a[href]:not([href=''])").each(function () {
  var $this = $(this);
  var $href = $this.attr("href");
  var $target = $this.attr('target');
  $href == $locationPathname && $this.addClass($classNameActive + ' ' + $classNameActiveLast);
  $this.click(function(){typeof $target !== 'undefined' ? window.open($href, $target) : window.location.href = $href});
  $('.js-endpoint.' + $classNameActiveLast).parentsUntil($sidebarContainerMenu).addClass('open').children('ul').show();
});

function ipnbarActiveAnchor(el) {
  $ipnbarAnchorsItem.children( 'a' ).removeClass($classNameActive);
  el.addClass($classNameActive);
}

$("h2[class*='icon-']").each(function(i) {
  var waypoint, $el, $parent, $id, $link, $offsetPx, $elLast, $elLastId, waypointLast, $elLastIdParent;
  $el = $(this);
  $id = $el.attr('id');
  $link = $( "a[href='#" + $id +"']" );
  $offsetPx = ($ipnbar.hasClass('headroom--pinned') || $ipnbar.hasClass('headroom--pinned--force') ? 144 : 216);
  $elLast = $( '.ipnbarAnchors li ').last().children( "a" );
  $elLastId = $elLast.attr('href');
  $elLastIdParent = $($elLastId).parent('section');

  if($el.length && $ipnbar.length){
    new Waypoint({
      element: $el,
      handler: function(){ipnbarActiveAnchor($link);},
      offset: $offsetPx*$fontSizeFactor()
    });
    new Waypoint({
      element: $elLastIdParent,
      handler: function(){ipnbarActiveAnchor($elLast)},
      offset: 'bottom-in-view'
    });
  }

});

$('.waypoint-topbar').each(function(i) {
  var $el, animClassDown, animClassUp, $offsetPx;
  $el = $(this);
  animClassDown = $el.data('animateDown');
  animClassUp = $el.data('animateUp');
  $offsetPx = 72;

  if($el.length && $ipnbar.length){
    $el.waypoint((function(direction) {
      var $bodyHeight = document.body.clientHeight;
      var $bodyScrollTop = document.body.scrollTop;
      var $clientHeight = document.documentElement.clientHeight;
      //var $heightLeft = *$fontSizeFactor();

      if (direction === 'down' && animClassDown && $bodyHeight - $bodyScrollTop - $offsetPx*$fontSizeFactor() > $clientHeight) {
        $ipnbar.addClass(animClassDown);
        if (animClassUp) {
          $ipnbar.removeClass(animClassUp);
        }
      } else if (direction === 'up' && animClassUp) {
        $ipnbar.addClass(animClassUp);
        if (animClassDown) {
          $ipnbar.removeClass(animClassDown);
        }
      }
    }), {
      offset: 0
    });

    $el.waypoint((function(direction) {
      if (direction === 'down' && animClassDown) {
        $topbar.addClass(animClassDown);
        if (animClassUp) {
          $topbar.removeClass(animClassUp);
        }
      } else if (direction === 'up' && animClassUp) {
        $topbar.addClass(animClassUp);
        $ipnbar.addClass(animClassUp);
        if (animClassDown) {
          $topbar.removeClass(animClassDown);
        }
      }
    }), {
      offset: $offsetPx*2*$fontSizeFactor()
    });
  }

});

$bars.hover(
  function(){$(this).addClass('headroom--pinned--force')},
  function(){$(this).removeClass('headroom--pinned--force')}
);

function headroomInit(el){
  var $el = document.querySelector(el);
  if($el){
    new Headroom($el, {
      "offset": $fontSizeFactor()*72,
      "tolerance": 5,
      "classes": {
        "pinned": "headroom--pinned",
        "unpinned": "headroom--unpinned"
      }
    }).init();
  }
}

headroomInit('#topbar');
headroomInit('#ipnbar');

$('.js-svg-pie-callouts use').each(function(){
  var $el = $(this);
  var $x = $el.attr('x');
  var $y = $el.attr('y');
  var $rotate = $el.attr('r');
  //var $width = $el.attr('width');
  //var $height = $el.attr('height');
  $el
    .removeAttr('x')
    .removeAttr('y')
    .removeAttr('r')
    .queue(function(){
      if($rotate){
        $(this).attr('transform', 'rotate(' + $rotate + ')')
      }
    })
    .wrap(
      '<svg class="solution-pie-callout js-solution-pie-callout" x="' + $x + '" y="' + $y + '" width="24" height="24" overflow="visible"></svg>'
    )
  ;
});

$('.js-solution-pie-callout use').bind('click mouseover mouseout', function() {
  var $el = $(this);
  var $idx = parseInt( $el.attr('data-index') );
  var $desc = $el.attr('data-desc');
  var $heroImgCalloutNumber = $('.heroImgCalloutNumber');
  var $heroImgCalloutText = $('.heroImgCalloutText');
  $heroImgCalloutNumber.text($idx).fadeIn();
  $heroImgCalloutText.text($desc).fadeIn();
});


$('.js-ipnbarAnchorsItemLink').bind('click', function(event) {
  var $anchor = $(this);
  var $delay = 600 ;
  var $offset = ($ipnbar.hasClass('waypoint-collapsed') ? 96*$fontSizeFactor() : 96*2*$fontSizeFactor());
  $('html, body').animate({scrollTop: $($anchor.attr('href')).offset().top - $offset}, $delay, 'swing');
  //ipnbarActiveAnchor($anchor);
  setTimeout(function() {ipnbarActiveAnchor($anchor)}, $delay);
  event.preventDefault();
});

$('[data-img]').each(function(){
  $(this).css('background-image', 'url(' + $(this).attr('data-img') + ')')
});

$(".catalog .previewImg").lazyload({
  effect : "fadeIn"
});

//$('.preview .previewTitle, .preview .previewDesc, .hero .headerSubtitle').each(function(){
//  $(this).multiLineEllipsis();
//});

// For remove :hover css style on touch devices
//if (!window.ontouchstart && !navigator.MaxTouchPoints && !navigator.msMaxTouchPoints) {
//  $body.addClass('notouch');
//}

if($ipnbar.hasClass('ipnbar-without-cta')){
  $body.addClass('ipnbar-without-cta')
}