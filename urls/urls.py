from django.views.generic.base import TemplateView
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from index.views import Index, ContactForm
import debug_toolbar



urlpatterns = [
    url(r'^$', Index.as_view(), {}, 'index'),
    url(r'^membrane/', include('membrane.urls')),
    url(r'^solution/', include('solution.urls')),
    url(r'^accessory/', include('accessory.urls')),
    url(r'^article/', include('article.urls')),
    url(r'^object/', include('object.urls')),
    url(r'^video/', include('video.urls')),
    url(r'^doc/', include('document.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^c_admin/', include('c_admin.urls')),
    url(r'^carousel/', include('carousel.urls')),
    url(r'^s/', TemplateView.as_view(template_name='google.html')),
    url(r'^contacts/', TemplateView.as_view(template_name='contacts.html')),
    url(r'^contacts_post/', ContactForm.as_view()),
    url(r'^about/', TemplateView.as_view(template_name='about.html')),
    url(r'^mission/', TemplateView.as_view(template_name='mission.html')),
    url(r'^production/', TemplateView.as_view(template_name='production.html')),
    url(r'^trainings/', TemplateView.as_view(template_name='trainings.html')),
    url(r'^partners/', TemplateView.as_view(template_name='partners.html')),
    url(r'^solution-calc/', TemplateView.as_view(template_name='solution-calc.html')),
    url(r'^solution-choice/', TemplateView.as_view(template_name='solution-choice.html')),
    url(r'^404/', TemplateView.as_view(template_name='404.html')),
    url(r'^images/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^__debug__/', include(debug_toolbar.urls)),
]