from django.contrib import admin
from .models import Video, VideoSubject


admin.site.register(Video)
admin.site.register(VideoSubject)