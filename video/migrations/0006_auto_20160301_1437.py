# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('video', '0005_auto_20160207_1931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='subject',
            field=models.ForeignKey(blank=True, to='video.VideoSubject', null=True),
        ),
    ]
