# -*- coding: utf-8 -*-
from django.db import models


class VideoSubject(models.Model):
    title = models.CharField(u'Название', max_length=255)

    def __unicode__(self):
        return self.title


class Video(models.Model):
    title = models.CharField(u'Название', max_length=255)
    desc = models.TextField(u'Базовое описание', blank=True)
    order = models.IntegerField(u'Порядок сортировки')
    subject = models.ForeignKey(VideoSubject, blank=True, null=True)
    url = models.URLField(u'Ссылка на видео')
    add = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['order']