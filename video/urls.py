from django.conf.urls import url
from .views import VideoList


urlpatterns = [
    url(r'^$', VideoList.as_view(), {}, 'video'),
    url(r'^ll/(?P<o>[0-9]+)/(?P<l>[0-9]+)/$', VideoList.as_view(template_name='video/ll.html'), {},
        'video'),
]
