from django.views.generic import View
from .models import Video
from django.shortcuts import render


class VideoList(View):
    template_name = 'video/index.html'

    def get(self, request, l=12, o=0):
        l = int(l)
        o = int(o)
        context = dict()
        objects = Video.objects.filter()
        context['items'] = objects[o:o+l]
        return render(request, self.template_name, context)